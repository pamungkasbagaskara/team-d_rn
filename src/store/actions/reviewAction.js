import { Alert } from 'react-native';

import { addReviewAPI, getReviewAPI } from './../../api/review';

export const addReview = (val) => {
  return async (dispatch) => {
    dispatch({
      type: 'POST_REVIEW_REQUEST'
    });
    try {
      const result = await addReviewAPI(val);
      if (result.status === 200) {
        dispatch({
          type: 'POST_REVIEW_SUCCESS',
          payload: result.data.result
        });
      } else {
        dispatch({
          type: 'POST_REVIEW_FAILED',
          error: result.data
        });
      }
    } catch (err) {
      dispatch({
        type: 'POST_REVIEW_FAILED',
        error: err
      });
      Alert.alert('Review untuk item ini sudah pernah ditulis!');
    }
  };
};


export const getDataReview = (id) => {
  return async (dispatch) => {
    dispatch({
      type: 'GET_REVIEW_REQUEST'
    });
    try {
      const result = await getReviewAPI(id);
      if (result.status === 200) {
        dispatch({
          type: 'GET_REVIEW_SUCCESS',
          payload: result.data.result
        });
      } else {
        dispatch({
          type: 'GET_REVIEW_FAILED',
          error: result.data
        });
        Alert.alert(result);
      }
    } catch (err) {
      dispatch({
        type: 'GET_REVIEW_FAILED',
        error: err
      });
    }
  };
};
