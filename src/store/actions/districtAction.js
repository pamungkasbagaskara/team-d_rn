import { ToastAndroid } from 'react-native';

import { getDataDistrict } from '../../api/address';

export const getDistrict = (id_regency) => {
  return async (dispatch) => {
    dispatch({
      type: 'GET_LIST_DISTRICT_REQUEST'
    });
    try {
      const result = await getDataDistrict(id_regency);

      if (result.status === 200) {
        dispatch({
          type: 'GET_LIST_DISTRICT_SUCCESS',
          payload: result.data.data.results
        });
      } else {
        dispatch({
          type: 'GET_LIST_DISTRICT_FAILED',
          error: result.data
        });
      }
    } catch (err) {
      dispatch({
        type: 'GET_LIST_DISTRICT_FAILED',
        error: err
      });

      ToastAndroid.showWithGravity(err.response.data.errorMessage, ToastAndroid.LONG, ToastAndroid.BOTTOM);
    }
  };
};

export const setDistrict = (district) => {
  return async (dispatch) => {
    dispatch({
      type: 'SELECT_DISTRICT',
      payload: district
    });
  };
};
