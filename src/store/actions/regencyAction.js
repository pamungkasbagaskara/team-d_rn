import { ToastAndroid } from 'react-native';

import { getDataRegency } from '../../api/address';

export const getRegency = (id_kota) => {
  return async (dispatch) => {
    dispatch({
      type: 'GET_LIST_REGENCY_REQUEST'
    });
    try {
      const result = await getDataRegency(id_kota);
      if (result.status === 200) {
        dispatch({
          type: 'GET_LIST_REGENCY_SUCCESS',
          payload: result.data.data.results
        });
      } else {
        dispatch({
          type: 'GET_LIST_REGENCY_FAILED',
          error: result.data
        });
      }
    } catch (err) {
      dispatch({
        type: 'GET_LIST_REGENCY_FAILED',
        error: err
      });
      ToastAndroid.showWithGravity(err.response.data.errorMessage, ToastAndroid.LONG, ToastAndroid.BOTTOM);
    }
  };
};

export const setRegency = (regency) => {
  return async (dispatch) => {
    dispatch({
      type: 'SELECT_REGENCY',
      payload: regency
    });
  };
};
