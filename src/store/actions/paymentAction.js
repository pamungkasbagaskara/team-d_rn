
import { ToastAndroid } from 'react-native';

import { doPayment, doVirtualPayment, uploadPaymentVerification, finishVirtualPayment } from '../../api/payment';
import * as RootNavigation from '../../navigation/RootNavigation';

export const pay = (transactionId, ammount, paymentMethodId) => {
  return async (dispatch) => {
    dispatch({
      type: 'PAYMENT_REQUEST'
    });

    try {
      const body = {
        'total_paid': ammount,
        'payment_method_id': paymentMethodId
      };
      const result = await doPayment(transactionId, body);
      if (result.status === 201) {
        dispatch({
          type: 'PAYMENT_SUCCESS',
          payload: result.data.result
        });
        RootNavigation.replace('PaymentCheck');
      } else {
        dispatch({
          type: 'PAYMENT_FAILED',
          error: result.data
        });
      }
    } catch (err) {
      ToastAndroid.showWithGravity(err.response.data.errorMessage, ToastAndroid.LONG, ToastAndroid.BOTTOM);
      dispatch({
        type: 'PAYMENT_FAILED',
        error: err
      });
    }
  };
};

export const virtualPay = (transactionId, ammount, virtualBank) => {
  return async (dispatch) => {
    dispatch({
      type: 'VIRTUAL_PAYMENT_REQUEST'
    });

    try {
      const body = {
        'amount': ammount,
        'idTransaction': transactionId,
        'bank': virtualBank
      };
      const result = await doVirtualPayment(body);
      if (result.status === 200) {
        dispatch({
          type: 'VIRTUAL_PAYMENT_SUCCESS',
          payload: result.data.result
        });
        RootNavigation.replace('VirtualPayment');
      } else {
        dispatch({
          type: 'VIRTUAL_PAYMENT_FAILED',
          error: result.data
        });
      }
    } catch (err) {
      ToastAndroid.showWithGravity(JSON.stringify(err.response), ToastAndroid.LONG, ToastAndroid.BOTTOM);
      dispatch({
        type: 'VIRTUAL_PAYMENT_FAILED',
        error: err
      });
    }
  };
};

export const confirm_payment = (transactionId, paymentMethodId, ammount, image) => {
  return async (dispatch) => {
    dispatch({
      type: 'CONFIRM_PAYMENT_REQUEST'
    });

    try {
      const imageProof = `data:image/jpeg;base64,${image.substring(0, 100)}`;
      const body = { transactionId, paymentMethodId, ammount, imageProof };
      const result = await uploadPaymentVerification(body);

      if (result.status === 201) {
        dispatch({
          type: 'CONFIRM_PAYMENT_SUCCESS',
          payload: result.data.result
        });
        RootNavigation.replace('Success');
      } else {
        dispatch({
          type: 'CONFIRM_PAYMENT_FAILED',
          error: result.data
        });
      }
    } catch (err) {
      dispatch({
        type: 'CONFIRM_PAYMENT_FAILED',
        error: err
      });
      ToastAndroid.showWithGravity(err.response.data.errorMessage, ToastAndroid.LONG, ToastAndroid.BOTTOM);
    }
  };
};

export const confirm_virtualPayment = (transactionId) => {
  return async (dispatch) => {
    dispatch({
      type: 'CONFIRM_VIRTUAL_PAYMENT_REQUEST'
    });

    try {
      const body = { 'idTransaction': transactionId };
      const result = await finishVirtualPayment(body);
      if (result.status === 200) {
        dispatch({
          type: 'CONFIRM_VIRTUAL_PAYMENT_SUCCESS'
        });
        RootNavigation.popToTop();
      } else {
        dispatch({
          type: 'CONFIRM_VIRTUAL_PAYMENT_FAILED',
          error: result.data
        });
      }
    } catch (err) {
      ToastAndroid.showWithGravity(JSON.stringify(err.response), ToastAndroid.LONG, ToastAndroid.BOTTOM);
      dispatch({
        type: 'CONFIRM_VIRTUAL_PAYMENT_FAILED',
        error: err
      });
    }
  };
};
