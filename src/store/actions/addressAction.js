import { Alert, ToastAndroid } from 'react-native';

import { getDataAddress, addDataAddress, deleteDataAddress, editDataAddress } from '../../api/address';
import * as RootNavigation from '../../navigation/RootNavigation';

export const getListAddress = () => {
  return async (dispatch) => {
    dispatch({
      type: 'GET_LIST_ADDRESS_REQUEST'
    });

    try {
      const result = await getDataAddress();
      if (result.status === 200) {
        dispatch({
          type: 'GET_LIST_ADDRESS_SUCCESS',
          payload: result.data.result
        });
      } else {
        dispatch({
          type: 'GET_LIST_ADDRESS_FAILED',
          error: result.data
        });
      }
    } catch (err) {
      dispatch({
        type: 'GET_LIST_ADDRESS_FAILED',
        error: err
      });
      ToastAndroid.showWithGravity(err.response.data.errorMessage, ToastAndroid.LONG, ToastAndroid.BOTTOM);
    }
  };
};

export const inputAddress = (body) => {
  return async (dispatch) => {
    dispatch({
      type: 'INPUT_ADDRESS_REQUEST'
    });

    try {
      const result = await addDataAddress(body);
      if (result.status === 201) {
        dispatch({
          type: 'INPUT_ADDRESS_SUCCESS',
          payload: result.data.result
        });
        RootNavigation.replace('Checkout');
        ToastAndroid.showWithGravity('Alamat berhasil ditambahkan', ToastAndroid.LONG, ToastAndroid.BOTTOM);
      } else {
        dispatch({
          type: 'INPUT_ADDRESS_FAILED',
          error: result.data
        });
      }
    } catch (err) {
      dispatch({
        type: 'INPUT_ADDRESS_FAILED',
        error: err
      });
      ToastAndroid.showWithGravity(err.response.data.errorMessage, ToastAndroid.LONG, ToastAndroid.BOTTOM);
    }
  };
};

export const selectAddress = (id) => {
  return async (dispatch) => {
    dispatch({
      type: 'SELECT_ADDRESS',
      payload: id
    });
  };
};

export const selectEditAddress = (body) => {
  return async (dispatch) => {
    dispatch({
      type: 'SELECT_EDIT_ADDRESS',
      payload: body
    });

    RootNavigation.navigate('EditAddress');
  };
};

export const editAddressAction = (body) => {
  return async (dispatch) => {
    dispatch({
      type: 'EDIT_ADDRESS_REQUEST'
    });

    try {
      const result = await editDataAddress(body);
      if (result.status === 201) {
        dispatch({
          type: 'EDIT_ADDRESS_SUCCESS',
          payload: result.data.result
        });
        RootNavigation.pop();
      } else {
        dispatch({
          type: 'EDIT_ADDRESS_FAILED',
          error: result.data
        });
      }
    } catch (err) {
      dispatch({
        type: 'EDIT_ADDRESS_FAILED',
        error: err
      });
      ToastAndroid.showWithGravity(err.response.data.errorMessage, ToastAndroid.LONG, ToastAndroid.BOTTOM);
    }
  };
};

export const deleteAddress = (id) => {
  return async (dispatch) => {
    dispatch({
      type: 'DELETE_ADDRESS_REQUEST'
    });

    try {
      const result = await deleteDataAddress(id);
      if (result.status === 200) {
        dispatch({
          type: 'DELETE_ADDRESS_SUCCESS',
          payload: result.data.result
        });
        RootNavigation.replace('Checkout');
        ToastAndroid.showWithGravity('Alamat berhasil dihapus', ToastAndroid.LONG, ToastAndroid.BOTTOM);
      }
      else {
        dispatch({
          type: 'DELETE_ADDRESS_FAILED',
          error: result.data
        });
      }
    } catch (err) {
      if (err.response.status === 500) {
        Alert.alert('Ooopss', 'Alamat ini tidak bisa dihapus karena sudah tercatat memiliki order');
      }
      dispatch({
        type: 'DELETE_ADDRESS_FAILED',
        error: err
      });
    }
  };
};
