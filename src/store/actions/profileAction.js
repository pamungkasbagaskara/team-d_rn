import { Alert } from 'react-native';

import { changeSIMAction } from '../actions/productAction';
import * as RootNavigation from '../../navigation/RootNavigation';
import { getProfile, patchProfile, register } from '../../api/profile';

export const getDataProfile = () => {
  return async (dispatch) => {
    dispatch({
      type: 'GET_PROFILE_REQUEST'
    });

    try {
      const result = await getProfile();
      if (result.status === 200) {
        dispatch({
          type: 'GET_PROFILE_SUCCESS',
          payload: result.data.result
        });
      } else {
        dispatch({
          type: 'GET_PROFILE_FAILED',
          error: result.data
        });
      }
    } catch (err) {
      dispatch({
        type: 'GET_PROFILE_FAILED',
        error: err
      });
    }
  };
};

export const goUpdate = (val) => {

  return async (dispatch) => {
    dispatch({
      type: 'UPDATE_REQUEST'
    });
    try {
      const result = await patchProfile(val);
      if (result.status === 201) {
        dispatch({
          type: 'UPDATE_SUCCESS',
          payload: result.data.result
        });
        Alert.alert('Ubah Profil Sukses');
      }
      else {
        dispatch({
          type: 'UPDATE_FAILED',
          error: result.data
        });
        Alert.alert(result.data.errorMessage);
      }
    } catch (error) {
      dispatch({
        type: 'UPDATE_FAILED',
        error: error
      });
    }
  };
};

export const goRegister = (val) => {

  return async (dispatch) => {
    dispatch({
      type: 'REGISTER_REQUEST'
    });
    try {
      const result = await register(val);
      if (result.status === 401) {
        dispatch({
          type: 'REGISTER_SUCCESS',
          payload: result.data.result
        });
        RootNavigation.replace('Login');
        Alert.alert('Registrasi Sukses');
      }
      else {
        dispatch({
          type: 'REGISTER_FAILED',
          error: result.data
        });
        Alert.alert('Registrasi tidak berhasil');
        RootNavigation.replace('Registers');
      }
    } catch (error) {
      dispatch({
        type: 'REGISTER_FAILED',
        error: error
      });
      RootNavigation.replace('Login');
      if (error.response.status === 400){
        Alert.alert('Email sudah digunakan, silahkan login!');
      }
      else {
        Alert.alert('Registrasi Sukses!, Aktivasi akun anda melalui email');}
    }
  };
};


export const goUpdateThenGantiSIM = (addBodyProfile, bodyChangeSIM) => {

  return async (dispatch) => {
    dispatch({
      type: 'UPDATE_REQUEST'
    });
    try {
      const result = await patchProfile(addBodyProfile);
      if (result.status === 201) {
        dispatch(changeSIMAction(bodyChangeSIM));
        dispatch({
          type: 'UPDATE_SUCCESS',
          payload: result.data.result
        });
      }
      else {
        dispatch({
          type: 'UPDATE_FAILED',
          error: result.data
        });
      }
    } catch (error) {
      dispatch({
        type: 'UPDATE_FAILED',
        error: error
      });
    }
  };
};


