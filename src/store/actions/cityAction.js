import { ToastAndroid } from 'react-native';

import { getDataCity } from '../../api/address';

export const getCity = (id) => {
  return async (dispatch) => {
    dispatch({
      type: 'GET_LIST_CITY_REQUEST'
    });

    try {
      const body = { provinceId: id };
      const result = await getDataCity(body);
      if (result.status === 200) {
        dispatch({
          type: 'GET_LIST_CITY_SUCCESS',
          payload: result.data.result
        });
      } else {
        dispatch({
          type: 'GET_LIST_CITY_FAILED',
          error: result.data
        });
      }
    } catch (err) {
      dispatch({
        type: 'GET_LIST_CITY_FAILED',
        error: err
      });
      ToastAndroid.showWithGravity(err.response.data.errorMessage, ToastAndroid.LONG, ToastAndroid.BOTTOM);
    }
  };
};

export const setCity = (city) => {
  return async (dispatch) => {
    dispatch({
      type: 'SELECT_CITY',
      payload: city
    });
  };
};
