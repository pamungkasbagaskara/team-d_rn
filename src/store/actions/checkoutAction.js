import { checkoutDataOrder } from '../../api/transaction';
import * as RootNavigation from '../../navigation/RootNavigation';

export const checkoutOrder = (transaction, address_id) => {
  return async (dispatch) => {
    dispatch({
      type: 'CHECKOUT_ORDER_REQUEST'
    });

    try {
      const result = await checkoutDataOrder(transaction, address_id);
      if (result.status === 201) {
        dispatch({
          type: 'CHECKOUT_ORDER_SUCCESS'
        });
        RootNavigation.replace('Payment');
      } else {
        dispatch({
          type: 'CHECKOUT_ORDER_FAILED',
          error: result.data
        });
      }
    } catch (err) {
      dispatch({
        type: 'CHECKOUT_ORDER_FAILED',
        error: err
      });
    }
  };
};
