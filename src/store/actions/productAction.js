import { Alert } from 'react-native';

import { getProduct, addToCart, getMsisdn, changeSIMAPI, getPrice } from '../../api/product';
import * as RootNavigation from '../../navigation/RootNavigation';

export const getDataProduct = (subProduct) => {
  return async (dispatch) => {
    dispatch({
      type: 'GET_PRODUCT_REQUEST'
    });

    try {
      const result = await getProduct(subProduct);
      if (result.status === 200) {
        dispatch({
          type: 'GET_PRODUCT_SUCCESS',
          payload: result.data
        });
      } else {
        dispatch({
          type: 'GET_PRODUCT_FAILED',
          error: result.data
        });
      }
    } catch (err) {
      dispatch({
        type: 'GET_PRODUCT_FAILED',
        error: err
      });
    }
  };
};

export const getDataMsisdn = (product, subProduct, digit) => {
  return async (dispatch) => {
    dispatch({
      type: 'GET_MSISDN_REQUEST'
    });

    try {
      const result = await getMsisdn(product, subProduct, digit);
      if (result.status === 200) {
        dispatch({
          type: 'GET_MSISDN_SUCCESS',
          payload: result.data
        });
      } else {
        dispatch({
          type: 'GET_MSISDN_FAILED',
          error: result.data
        });
      }
    } catch (err) {
      dispatch({
        type: 'GET_MSISDN_FAILED',
        error: err
      });
    }
  };
};

export const inputDetilProduct = (item) => {
  return async (dispatch) => {
    dispatch({
      type: 'INPUT_DETILPRODUCT_REQUEST',
      payload: item
    });
    RootNavigation.push('Detil');
  };
};

export const navigateBundling = (tab) => {
  return async (dispatch) => {
    dispatch({
      type: 'NAVIGATE_BUNDLING_REQUEST',
      payload: tab
    });
    RootNavigation.push('Bundling');
  };
};

export const navigatePascabayar = (tab) => {
  return async (dispatch) => {
    dispatch({
      type: 'NAVIGATE_PASCABAYAR_REQUEST',
      payload: tab
    });
    RootNavigation.push('Pascabayar');
  };
};

export const navigateStarterpack = (tab) => {
  return async (dispatch) => {
    dispatch({
      type: 'NAVIGATE_STARTERPACK_REQUEST',
      payload: tab
    });
    RootNavigation.push('StarterPack');
  };
};

export const inputMsisdn = (item) => {
  return async (dispatch) => {
    dispatch({
      type: 'INPUT_MSISDN_REQUEST',
      payload: item
    });
    RootNavigation.navigate('Msisdn');
  };
};


export const addCart = (id, nbr) => {
  return async (dispatch) => {
    dispatch({
      type: 'ADD_TO_CART_REQUEST'
    });

    try {
      const result = await addToCart(id, nbr);
      if (result.status === 201) {
        dispatch({
          type: 'ADD_TO_CART_SUCCESS',
          payload: result.data
        });

        Alert.alert(
          'Sukses',
          'Barang Pindah Ke Keranjang',
          [{ text: 'OK', onPress: () => RootNavigation.navigate('Cart') }],
          { cancelable: false },
        );
      } else {
        dispatch({
          type: 'ADD_TO_CART_FAILED'
        });
      }
    } catch (err) {
      dispatch({
        type: 'ADD_TO_CART_FAILED',
        error: err
      });
    }
  };
};

export const changeSIMAction = (body) => {
  return async (dispatch) => {
    dispatch({
      type: 'CHANGE_SIM_REQUEST'
    });

    try {
      const result = await changeSIMAPI(body);
      if (result.status === 201) {
        dispatch({
          type: 'CHANGE_SIM_SUCCESS',
          payload: result.data
        });

        Alert.alert(
          'Sukses',
          'Barang Pindah Ke Keranjang',
          [{ text: 'OK', onPress: () => RootNavigation.navigate('Cart') }],
          { cancelable: false },
        );
      } else {
        dispatch({
          type: 'CHANGE_SIM_FAILED'
        });
      }
    } catch (err) {
      dispatch({
        type: 'CHANGE_SIM_FAILED',
        error: err
      });
      Alert.alert(err.response.data.errorMessage);
    }
  };
};


export const getDataPrice = () => {
  return async (dispatch) => {
    dispatch({
      type: 'GET_MINIMUM_PRICE_REQUEST'
    });

    try {
      const result = await getPrice();
      if (result.status === 200) {
        dispatch({
          type: 'GET_MINIMUM_PRICE_SUCCESS',
          payload: result.data
        });
      } else {
        dispatch({
          type: 'GET_MINIMUM_PRICE_FAILED',
          error: result.data
        });
      }
    } catch (err) {
      dispatch({
        type: 'GET_MINIMUM_PRICE_FAILED',
        error: err
      });
    }
  };
};
