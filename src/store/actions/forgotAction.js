import AsyncStorage from '@react-native-async-storage/async-storage';
import { Alert } from 'react-native';

import * as RootNavigation from '../../navigation/RootNavigation';
import { changePassword, forgot, sendOTP, validate_otp } from '../../api/forgot';

export const goForgot = (OTP) => {
  return async (dispatch) => {
    dispatch({
      type: 'FORGOT_REQUEST'
    });

    try {
      const body = { OTP };
      const result = await forgot(body);
      if (result.status === 201) {
        dispatch({
          type: 'FORGOT_SUCCESS',
          payload: result.data.result
        });
      }
      else {
        dispatch({
          type: 'FORGOT_FAILED',
          error: result.data
        });
        Alert.alert('Kode OTP salah');
      }
    } catch (err) {
      dispatch({
        type: 'FORGOT_FAILED',
        error: err
      });
    }
  };
};

export const goValidate = (email, token) => {
  const forgotToken = async (val) => {
    try {
      await AsyncStorage.setItem('forgot_token', val);
      return true;
    } catch (e) {
      Alert.alert('Store token eror');
    }
  };

  return async (dispatch) => {
    dispatch({
      type: 'VALIDATE_REQUEST'
    });

    try {
      const body = { email, token };
      const result = await validate_otp(body);
      if (result.status === 201) {
        dispatch({
          type: 'VALIDATE_SUCCESS',
          payload: result.data.result
        });
        forgotToken(result.data.result.token);
        RootNavigation.navigate('ChangePassword');
      }
      else {
        dispatch({
          type: 'VALIDATE_FAILED',
          error: result.data
        });
        Alert.alert('OTP Anda salah');
      }
    } catch (err) {
      dispatch({
        type: 'VALIDATE_FAILED',
        error: err
      });
      Alert.alert('OTP Anda salah');
    }
  };
};

export const goOTP = (val) => {
  return async (dispatch) => {
    dispatch({
      type: 'OTP_REQUEST'
    });

    try {
      const body = (val);
      const result = await sendOTP(body);
      if (result.status === 201) {
        dispatch({
          type: 'OTP_SUCCESS',
          payload: result.data.result
        });
      }
      else {
        dispatch({
          type: 'OTP_FAILED',
          error: result.data
        });
      }
    } catch (err) {
      dispatch({
        type: 'OTP_FAILED',
        error: err
      });
    }
  };
};

export const goChangePassword = (val) => {
  return async (dispatch) => {
    dispatch({
      type: 'CHANGE_PASSWORD_REQUEST'
    });

    try {
      const result = await changePassword(val);
      if (result.status === 201) {
        dispatch({
          type: 'CHANGE_PASSWORD_SUCCESS',
          payload: result.data.result
        });
        Alert.alert('Ganti Password Sukses');
        RootNavigation.replace('Login');
      }
      else {
        dispatch({
          type: 'CHANGE_PASSWORD_FAILED',
          error: result.data
        });
      }
    } catch (err) {
      dispatch({
        type: 'CHANGE_PASSWORD_FAILED',
        error: err
      });
    }
  };
};
