
import { ToastAndroid } from 'react-native';

import { getDataProvince } from './../../api/address';

export const getProvince = () => {
  return async (dispatch) => {
    dispatch({
      type: 'GET_LIST_PROVINCE_REQUEST'
    });

    try {
      const result = await getDataProvince();
      if (result.status === 200) {
        dispatch({
          type: 'GET_LIST_PROVINCE_SUCCESS',
          payload: result.data.result
        });
      } else {
        dispatch({
          type: 'GET_LIST_PROVINCE_FAILED',
          error: result.data
        });
      }
    } catch (err) {
      dispatch({
        type: 'GET_LIST_PROVINCE_FAILED',
        error: err
      });
      ToastAndroid.showWithGravity(err.response.data.errorMessage, ToastAndroid.LONG, ToastAndroid.BOTTOM);
    }
  };
};

export const setProvince = (province) => {
  return async (dispatch) => {
    dispatch({
      type: 'SELECT_PROVINCE',
      payload: province
    });
  };
};
