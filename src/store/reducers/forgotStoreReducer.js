const initialState = {
  token: null,
  refresh_token: null,
  isLoading: false,
  otp: null
};

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case 'FORGOT_REQUEST': {
      return {
        ...state,
        isLoading: true
      };
    }
    case 'FORGOT_SUCCESS': {
      return {
        ...state,
        status: action.payload,
        isLoading: false
      };
    }
    case 'FORGOT_FAILED': {
      return {
        ...state,
        isLoading: false
      };
    }
    case 'VALIDATE_REQUEST': {
      return {
        ...state,
        isLoading: true
      };
    }
    case 'VALIDATE_SUCCESS': {
      return {
        ...state,
        token: action.payload.token,
        refresh_token: action.payload.refresh_token,
        isLoading: false
      };
    }
    case 'VALIDATE_FAILED': {
      return {
        ...state,
        isLoading: false
      };
    }
    case 'OTP_REQUEST': {
      return {
        ...state,
        isLoading: true
      };
    }
    case 'OTP_SUCCESS': {
      return {
        ...state,
        otp: action.payload,
        isLoading: false
      };
    }
    case 'OTP_FAILED': {
      return {
        ...state,
        isLoading: false
      };
    }
    case 'CHANGE_PASSWORD_REQUEST': {
      return {
        ...state,
        isLoading: true
      };
    }
    case 'CHANGE_PASSWORD_SUCCESS': {
      return {
        ...state,
        otp: action.payload,
        isLoading: false
      };
    }
    case 'CHANGE_PASSWORD_FAILED': {
      return {
        ...state,
        isLoading: false
      };
    }
    default:
      return state;
  }
};
