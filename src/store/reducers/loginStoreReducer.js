const initialState = {
  email: null,
  password: null,
  token: null,
  refresh_token: null,
  isLoading: false
};

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case 'LOGIN_REQUEST': {
      return {
        ...state,
        isLoading: true
      };
    }
    case 'LOGIN_SUCCESS': {
      return {
        ...state,
        token: action.payload.token,
        refresh_token: action.payload.refresh_token,
        isLoading: false
      };
    }
    case 'LOGIN_FAILED': {
      return {
        ...state,
        isLoading: false
      };
    }
    case 'REFRESH_REQUEST': {
      return {
        ...state,
        isLoading: true
      };
    }
    case 'REFRESH_SUCCESS': {
      return {
        ...state,
        token: action.payload.token,
        refresh_token: action.payload.refresh_token,
        isLoading: false
      };
    }
    case 'REFRESH_FAILED': {
      return {
        ...state,
        isLoading: false
      };
    }
    default:
      return state;
  }
};
