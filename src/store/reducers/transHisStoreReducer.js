const initialState = {
  dataTransaction: [],
  isLoading: false
};

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case 'GET_TRANSACTION_HISTORY_REQUEST': {
      return {
        ...state,
        isLoading: true
      };
    }
    case 'GET_TRANSACTION_HISTORY_SUCCESS': {
      return {
        ...state,
        dataTransaction: action.payload,
        isLoading: false
      };
    }
    case 'GET_TRANSACTION_HISTORY_FAILED': {
      return {
        ...state,
        isLoading: false
      };
    }
    default:
      return state;
  }
};

