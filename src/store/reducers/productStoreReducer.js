const initialState = {
  product: [],
  price: [],
  detilProduct: [],
  msisdn: [],
  nbr: [],
  tab: [0],
  isLoading: true,
  bin: false
};

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case 'GET_PRODUCT_REQUEST': {
      return {
        ...state,
        isLoading: true
      };
    }
    case 'GET_PRODUCT_SUCCESS': {
      return {
        ...state,
        product: action.payload,
        isLoading: false
      };
    }
    case 'GET_PRODUCT_FAILED': {
      return {
        ...state,
        isLoading: false
      };
    }
    case 'INPUT_DETILPRODUCT_REQUEST': {
      return {
        ...state,
        detilProduct: action.payload
      };
    }
    case 'INPUT_MSISDN_REQUEST': {
      return {
        ...state,
        msisdn: action.payload
      };
    }

    case 'GET_MSISDN_REQUEST': {
      return {
        ...state,
        isLoading: true,
        bin: true
      };
    }
    case 'GET_MSISDN_SUCCESS': {
      return {
        ...state,
        nbr: action.payload,
        isLoading: false,
        bin: true
      };
    }
    case 'GET_MSISDN_FAILED': {
      return {
        ...state,
        isLoading: false,
        bin: false
      };
    }
    case 'GET_MINIMUM_PRICE_REQUEST': {
      return {
        ...state,
        isLoading: true
      };
    }
    case 'GET_MINIMUM_PRICE_SUCCESS': {
      return {
        ...state,
        price: action.payload,
        isLoading: false
      };
    }
    case 'GET_MINIMUM_PRICE_FAILED': {
      return {
        ...state,
        isLoading: false
      };
    }
    case 'NAVIGATE_BUNDLING_REQUEST': {
      return {
        ...state,
        tab: action.payload
      };
    }
    case 'NAVIGATE_PASCABAYAR_REQUEST': {
      return {
        ...state,
        tab: action.payload
      };
    }
    case 'NAVIGATE_STARTERPACK_REQUEST': {
      return {
        ...state,
        tab: action.payload
      };
    }
    default:
      return state;
  }
};


