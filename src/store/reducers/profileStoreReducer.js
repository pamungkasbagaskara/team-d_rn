const initialState = {
  profile: [],
  isLoading: false,
  isUpdatingProfile: false
};

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case 'GET_PROFILE_REQUEST': {
      return {
        ...state,
        isLoading: true
      };
    }
    case 'GET_PROFILE_SUCCESS': {
      return {
        ...state,
        profile: action.payload,
        isLoading: false
      };
    }
    case 'GET_PROFILE_FAILED': {
      return {
        ...state,
        isLoading: false
      };
    }
    case 'UPDATE_REQUEST': {
      return {
        ...state,
        isLoading: true,
        isUpdatingProfile: true
      };
    }
    case 'UPDATE_SUCCESS': {
      return {
        ...state,
        profile: action.payload,
        isLoading: false,
        isUpdatingProfile: false
      };
    }
    case 'UPDATE_FAILED': {
      return {
        ...state,
        isLoading: false,
        isUpdatingProfile: false
      };
    }
    case 'REGISTER_REQUEST': {
      return {
        ...state,
        isLoading: true
      };
    }
    case 'REGISTER_SUCCESS': {
      return {
        ...state,
        isLoading: false
      };
    }
    case 'REGISTER_FAILED': {
      return {
        ...state,
        isLoading: false
      };
    }
    default:
      return state;
  }
};

