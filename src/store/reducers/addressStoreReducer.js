const initialState = {
  address: [],
  selectedAddress: '',
  defaultAddress: {},
  editAddress: {},
  isLoading: false,
  province: [],
  selectedProvince: {},
  city: [],
  selectedCity: {},
  regency: [],
  selectedRegency: {},
  district: [],
  selectedDistrict: {},
  long: null,
  lat: null,
  mylong: null,
  mylat: null
};

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case 'GET_LIST_ADDRESS_REQUEST': {
      return {
        ...state,
        isLoading: true
      };
    }
    case 'GET_LIST_ADDRESS_SUCCESS': {
      return {
        ...state,
        address: action.payload,
        defaultAddress: action.payload[0],
        isLoading: false
      };
    }
    case 'GET_LIST_ADDRESS_FAILED': {
      return {
        ...state,
        isLoading: false
      };
    }
    case 'INPUT_ADDRESS_REQUEST': {
      return {
        ...state,
        isLoading: true
      };
    }
    case 'INPUT_ADDRESS_SUCCESS': {
      return {
        ...state,
        isLoading: false,
        selectedAddress: action.payload,
        selectedProvince: {},
        selectedCity: {},
        selectedRegency: {},
        selectedDistrict: {},
        long: null,
        lat: null
      };
    }
    case 'INPUT_ADDRESS_FAILED': {
      return {
        ...state,
        isLoading: false
      };
    }
    case 'SELECT_ADDRESS': {
      return {
        ...state,
        selectedAddress: action.payload
      };
    }
    case 'GET_LIST_PROVINCE_REQUEST': {
      return {
        ...state,
        isLoading: true
      };
    }
    case 'GET_LIST_PROVINCE_SUCCESS': {
      return {
        ...state,
        isLoading: false,
        province: action.payload
      };
    }
    case 'GET_LIST_PROVINCE_FAILED': {
      return {
        ...state,
        isLoading: false
      };
    }
    case 'SELECT_PROVINCE': {
      return {
        ...state,
        selectedProvince: action.payload
      };
    }
    case 'GET_LIST_CITY_REQUEST': {
      return {
        ...state,
        isLoading: true
      };
    }
    case 'GET_LIST_CITY_SUCCESS': {
      return {
        ...state,
        isLoading: false,
        city: action.payload
      };
    }
    case 'GET_LIST_CITY_FAILED': {
      return {
        ...state,
        isLoading: false
      };
    }
    case 'SELECT_CITY': {
      return {
        ...state,
        selectedCity: action.payload
      };
    }
    case 'GET_LIST_REGENCY_SUCCESS': {
      return {
        ...state,
        isLoading: false,
        regency: action.payload
      };
    }
    case 'GET_LIST_REGENCY_FAILED': {
      return {
        ...state,
        isLoading: false
      };
    }
    case 'SELECT_REGENCY': {
      return {
        ...state,
        selectedRegency: action.payload
      };
    }

    case 'GET_LIST_DISTRICT_SUCCESS': {
      return {
        ...state,
        isLoading: false,
        district: action.payload
      };
    }
    case 'GET_LIST_DISTRICT_FAILED': {
      return {
        ...state,
        isLoading: false
      };
    }
    case 'SELECT_DISTRICT': {
      return {
        ...state,
        selectedDistrict: action.payload
      };
    }
    case 'SET_LONG_LAT': {
      return {
        ...state,
        long: action.payload.long,
        lat: action.payload.lat
      };
    }
    case 'SET_MY_LOCATION': {
      return {
        ...state,
        mylong: action.payload.long,
        mylat: action.payload.lat
      };
    }
    case 'SELECT_EDIT_ADDRESS': {
      return {
        ...state,
        editAddress: action.payload
      };
    }
    case 'EDIT_ADDRESS_REQUEST': {
      return {
        ...state,
        isLoading: true
      };
    }
    case 'EDIT_ADDRESS_SUCCESS': {
      return {
        ...state,
        isLoading: false,
        selectedAddress: action.payload,
        selectedProvince: {},
        selectedCity: {},
        selectedRegency: {},
        selectedDistrict: {},
        editAddress: {},
        long: null,
        lat: null
      };
    }
    case 'EDIT_ADDRESS_FAILED': {
      return {
        ...state,
        isLoading: false
      };
    }
    case 'DELETE_ADDRESS_REQUEST': {
      return {
        ...state,
        isLoading: true
      };
    }
    case 'DELETE_ADDRESS_SUCCESS': {
      return {
        ...state,
        isLoading: false,
        selectedAddress: {},
        selectedProvince: {},
        selectedCity: {},
        selectedRegency: {},
        selectedDistrict: {},
        editAddress: {},
        long: null,
        lat: null
      };
    }
    case 'DELETE_ADDRESS_FAILED': {
      return {
        ...state,
        isLoading: false
      };
    }
    default:
      return state;
  }
};

