const initialState = {
  order: {},
  current_transaction: null,
  total_transaction: 0,
  weight: 0,
  shipmentPrice: 0,
  isLoading: false,
  isUpdatingOrder: false
};

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case 'GET_LIST_ORDER_REQUEST': {
      return {
        ...state,
        isLoading: true
      };
    }
    case 'GET_LIST_ORDER_SUCCESS': {
      if (action.payload) {
        return {
          ...state,
          order: action.payload,
          current_transaction: action.payload.id_transaction,
          shipmentPrice: action.payload.shipmentPrice,
          weight: action.payload.weight,
          total_transaction: action.payload.shipmentPrice + action.payload.totalPrice,
          isLoading: false
        };
      }
      else {
        return {
          ...state,
          order: {}
        };
      }
    }
    case 'GET_LIST_ORDER_FAILED': {
      return {
        ...state,
        isLoading: false
      };
    }
    case 'CHECKOUT_ORDER_REQUEST': {
      return {
        ...state,
        isLoading: true
      };
    }
    case 'CHECKOUT_ORDER_SUCCESS': {
      return {
        ...state,
        order: {},
        isLoading: false
      };
    }
    case 'CHECKOUT_ORDER_FAILED': {
      return {
        ...state,
        isLoading: false
      };
    }
    case 'ADD_QUANTITY_ORDER_REQUEST': {
      return {
        ...state,
        isLoading: true,
        isUpdatingOrder: true
      };
    }
    case 'ADD_QUANTITY_ORDER_SUCCESS': {
      return {
        ...state,
        isLoading: false,
        isUpdatingOrder: false
      };
    }
    case 'ADD_QUANTITY_ORDER_FAILED': {
      return {
        ...state,
        isLoading: false,
        isUpdatingOrder: false
      };
    }
    case 'SUBS_QUANTITY_ORDER_REQUEST': {
      return {
        ...state,
        isLoading: true,
        isUpdatingOrder: true
      };
    }
    case 'SUBS_QUANTITY_ORDER_SUCCESS': {
      return {
        ...state,
        isLoading: false,
        isUpdatingOrder: false
      };
    }
    case 'SUBS_QUANTITY_ORDER_FAILED': {
      return {
        ...state,
        isLoading: false,
        isUpdatingOrder: false
      };
    }
    case 'DEL_QUANTITY_ORDER_REQUEST': {
      return {
        ...state,
        isLoading: true,
        isUpdatingOrder: true
      };
    }
    case 'DEL_QUANTITY_ORDER_SUCCESS': {
      const updatedOrder = state.order.product.filter(item=> item.id !== action.product_id);
      return {
        ...state,
        order: updatedOrder,
        isLoading: false,
        isUpdatingOrder: false
      };
    }
    case 'DEL_QUANTITY_ORDER_FAILED': {
      return {
        ...state,
        isLoading: false,
        isUpdatingOrder: false
      };
    }
    case 'PROCESSED_FROM_HISTORY': {
      return {
        ...state,
        isLoading: false,
        current_transaction: action.id_transaction,
        total_transaction: action.totalPrice
      };
    }

    case 'GET_SHIPMENT_PRICE_REQUEST': {
      return {
        ...state,
        isLoading: true
      };
    }
    case 'GET_SHIPMENT_PRICE_SUCCESS': {
      return {
        ...state,
        shipmentPrice: action.payload,
        total_transaction: action.payload + state.order.totalPrice,
        isLoading: false
      };
    }
    case 'GET_SHIPMENT_PRICE_FAILED': {
      return {
        ...state,
        isLoading: false
      };
    }
    default:
      return state;
  }
};

