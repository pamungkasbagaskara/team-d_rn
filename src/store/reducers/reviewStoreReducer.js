const initialState = {
  review: [],
  dataReview: [],
  isLoading: true
};

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case 'POST_REVIEW_REQUEST': {
      return {
        ...state,
        isLoading: true
      };
    }
    case 'POST_REVIEW_SUCCESS': {
      return {
        ...state,
        review: action.payload,
        isLoading: false
      };
    }
    case 'POST_REVIEW_FAILED': {
      return {
        ...state,
        isLoading: false
      };
    }
    case 'GET_REVIEW_REQUEST': {
      return {
        ...state,
        isLoading: true
      };
    }
    case 'GET_REVIEW_SUCCESS': {
      return {
        ...state,
        dataReview: action.payload,
        isLoading: false
      };
    }
    case 'GET_REVIEW_FAILED': {
      return {
        ...state,
        isLoading: false
      };
    }
    default:
      return state;
  }
};

