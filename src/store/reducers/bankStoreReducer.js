const initialState = {
  bank: [],
  selectedBank: '',
  isLoading: false,
  isPayWithVirtualBank: false,
  virtualBank: null
};

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case 'GET_LIST_BANK_REQUEST': {
      return {
        ...state,
        isLoading: true
      };
    }
    case 'GET_LIST_BANK_SUCCESS': {
      return {
        ...state,
        bank: action.payload,
        isLoading: false
      };
    }
    case 'GET_LIST_BANK_FAILED': {
      return {
        ...state,
        isLoading: false
      };
    }
    case 'SELECT_BANK': {
      return {
        ...state,
        selectedBank: action.payload,
        isPayWithVirtualBank: action.isPayWithVirtualBank,
        virtualBank: action.virtualBank
      };
    }
    default:
      return state;
  }
};

