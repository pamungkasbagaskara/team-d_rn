import { fetchAPIProduction } from '../utils/fetchAPI';

export const getDataListBank = async () => {
  try {
    const result = await fetchAPIProduction('GET', 'payment/bank');
    return result;
  } catch (error) {
    throw error;
  }
};

export const doPayment = async (id, body) => {
  try {
    const result = await fetchAPIProduction('PATCH', `payment/pending?idTransaction=${id}`, body);
    return result;
  } catch (error) {
    throw error;
  }
};

export const doVirtualPayment = async (body) => {
  try {
    const result = await fetchAPIProduction('POST', 'payment/charge', body);
    return result;
  } catch (error) {
    throw error;
  }
};

export const uploadPaymentVerification = async (body) => {
  try {
    const result = await fetchAPIProduction('POST', 'payment/proof', body);
    return result;
  } catch (error) {
    throw error;

  }
};

export const finishVirtualPayment = async (body) => {
  try {
    const result = await fetchAPIProduction('PATCH', 'payment/status', body);
    return result;
  } catch (error) {
    throw error;

  }
};
