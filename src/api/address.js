import { fetchAPIProduction, fetchAPIWilayahIndonesia } from '../utils/fetchAPI';

export const getDataAddress = async () => {
  try {
    const result = await fetchAPIProduction('GET', 'user/address');
    return result;
  } catch (error) {
    throw error;
  }
};

export const addDataAddress = async (body) => {
  try {
    const result = await fetchAPIProduction('POST', 'user/address', body);
    return result;
  } catch (error) {
    throw error;
  }
};

export const editDataAddress = async (body) => {
  try {
    const result = await fetchAPIProduction('PATCH', 'user/address', body);
    return result;
  } catch (error) {
    throw error;
  }
};

export const deleteDataAddress = async (body) => {
  try {
    const result = await fetchAPIProduction('DELETE', 'user/address', body);
    return result;
  } catch (error) {
    throw error;
  }
};

export const getDataProvince = async () => {
  try {
    const result = await fetchAPIProduction('GET', 'shipping/province');
    return result;
  } catch (error) {
    throw error;
  }
};

export const getDataCity = async (body) => {
  try {
    const result = await fetchAPIProduction('POST', 'shipping/regency', body);
    return result;
  } catch (error) {
    throw error;
  }
};

export const getDataRegency = async (id_kota) => {
  try {
    const result = await fetchAPIWilayahIndonesia('GET', `districts?city=${id_kota}`);
    return result;
  } catch (error) {
    throw error;
  }
};

export const getDataDistrict = async (id_kecamatan) => {
  try {
    const result = await fetchAPIWilayahIndonesia('GET', `villages?district=${id_kecamatan}`);
    return result;
  } catch (error) {
    throw error;
  }
};
