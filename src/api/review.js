import { Alert } from 'react-native';

import { fetchAPIProduction, fetchReviewAPI } from '../utils/fetchAPI';

export const addReviewAPI = async (body) => {
  try {
    const result = await fetchAPIProduction('POST', 'product/review', body);
    Alert.alert('Review berhasil ditambahkan!');
    return result;
  } catch (error) {
    throw error;
  }
};

export const getReviewAPI = async (body) => {
  try {
    const bodyRequest = {
      id: `${body}`
    };
    const result = await fetchReviewAPI('GET', 'product', bodyRequest);
    return result;
  } catch (error) {
    throw error;
  }
};
