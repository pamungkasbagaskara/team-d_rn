import { fetchAPIProduction } from '../utils/fetchAPI';

export const login = async(body)=>{
  try {
    const path = 'auth/login';
    const result = await fetchAPIProduction('POST', path, body);
    return result;
  } catch (error) {
    return (error.response.data.errorMessage);
  }
};

export const refresh_token = async(body)=>{
  try {
    const path = 'auth/refresh';
    const result = await fetchAPIProduction('POST', path, body);
    return result;
  } catch (error) {
    return (error.response.data.errorMessage);
  }
};

