import { Alert } from 'react-native';

import { fetchAPIProduction } from '../utils/fetchAPI';

export const getProfile = async ()=>{
  try {
    const path = 'user/profile/';
    const result = await fetchAPIProduction('GET', path);
    return result;
  } catch (error) {
    throw error;
  }
};

export const patchProfile = async (body)=>{
  try {
    const path = 'user/profile/';
    const result = await fetchAPIProduction('PATCH', path, body);
    return result;
  } catch (error) {
    Alert.alert(error.response.data.errorMessage);
    throw error;
  }
};

export const register = async (body)=>{
  try {
    const path = 'auth/register/';
    const result = await fetchAPIProduction('POST', path, body);
    return result;
  } catch (error) {
    throw error;
  }
};
