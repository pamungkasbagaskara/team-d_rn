import { fetchAPIProduction, fetchChangePassword } from '../utils/fetchAPI';

export const forgot = async(body)=>{
  try {
    const path = 'auth/forgot';
    const result = await fetchAPIProduction('POST', path, body);
    return result;
  } catch (error) {
    throw error;
  }
};

export const validate_otp = async(body)=>{
  try {
    const path = 'auth/forgot/verify';
    const result = await fetchAPIProduction('POST', path, body);
    return result;
  } catch (error) {
    throw error;
  }
};

export const sendOTP = async (body) => {
  try {
    const path = 'auth/forgot';
    const result = await fetchAPIProduction('POST', path, body);
    return result;
  } catch (error) {
    return (error.response.data.errorMessage);
  }
};

export const changePassword = async (body) => {
  try {
    const path = 'user/changePassword';
    const result = await fetchChangePassword('PATCH', path, body);
    return result;
  } catch (error) {
    return (error.response.data.errorMessage);
  }
};
