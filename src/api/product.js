import { fetchAPIProduction } from '../utils/fetchAPI';

export const getProduct = async () => {
  try {
    const result = await fetchAPIProduction('GET', 'product/list');
    return result;
  } catch (error) {
    throw error;
  }
};

export const addToCart = async (body,nbr) => {
  try {
    const bodyRequest = {
      product_id: body,
      qty: 1,
      number: nbr
    };
    const result = await fetchAPIProduction('POST', 'transaction/cart', bodyRequest);
    return result;
  } catch (error) {
    throw error;
  }
};

export const getMsisdn = async (product, subProduct, digit) => {
  try {
    const bodyRequest = {
      productCategory: product,
      subProduct: subProduct,
      preferNumber: digit
    };
    const result = await fetchAPIProduction('GET', 'product/number', bodyRequest);
    return result;
  } catch (error) {
    throw error;
  }
};

export const changeSIMAPI = async (body) => {
  try {
    const result = await fetchAPIProduction('POST', 'transaction/cart', body);
    return result;
  } catch (error) {
    throw error;
  }
};

export const getPrice = async () => {
  try {
    const result = await fetchAPIProduction('GET', 'product/category');
    return result;
  } catch (error) {
    throw error;
  }
};
