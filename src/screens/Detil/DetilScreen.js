import React, { useEffect } from 'react';
import {
  ScrollView,
  View,
  Text,
  TouchableOpacity,
  Image,
  Alert
} from 'react-native';
import numbro from 'numbro';
import LinearGradient from 'react-native-linear-gradient';
import { useSelector, useDispatch } from 'react-redux';
import Ionicons from 'react-native-vector-icons/Ionicons';

import * as RootNavigation from '../../navigation/RootNavigation';
import { addCart, inputMsisdn } from '../../store/actions/productAction';
import { getDataReview } from '../../store/actions/reviewAction';
import { CartIcon, BackIcon } from '../../components/Icon';
import ProductCard from '../../components/ProductCard';

import styles from './DetilStyles';

export default function DetilScreen() {
  const item = useSelector((state) => state.productStore.detilProduct);
  const review = useSelector((state) => state.reviewStore.dataReview);
  const product = useSelector((state) => state.productStore.product);
  const dispatch = useDispatch();
  const ProdukTerkait = (data) => (
    <ScrollView horizontal={true}>
      <ProductCard subProduct={item.subProduct} product={data} />
    </ScrollView>
  );

  const confirm = () => {
    Alert.alert(
      'Cart',
      'Apakah Kamu Yakin Membeli Barang Ini?',
      [
        {
          text: 'Pilih Lainnya',
          style: 'cancel'
        },
        { text: 'Ya Bungkus', onPress: () => dispatch(addCart(item.id)) }
      ],
      { cancelable: false },
    );
  };

  useEffect(() => {
    dispatch(getDataReview(item.id));
  }, []);

  return (
    <>
      <LinearGradient
        start={{ x: 0, y: 0 }}
        end={{ x: 1, y: 0 }}
        colors={['#e52d27', '#b31217']}
        style={styles.linearGradient}>
        <View style={styles.container}>
          <ScrollView showsVerticalScrollIndicator={false}>
            <View style={styles.GantiHeader}>
              <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                <TouchableOpacity
                  style={{ backgroundColor: 'red', borderRadius: 100 }}
                  onPress={() => RootNavigation.pop()}>
                  <BackIcon />
                </TouchableOpacity>
                <Text style={styles.fontGanti}>Detil Produk</Text>
              </View>
            </View>
            <Image style={styles.poster} source={{ uri: item.poster }} />
            <Text style={styles.subProduct}>{item.subProduct}</Text>
            <Text style={styles.name} note numberOfLines={2}>
              {item.name}
            </Text>
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'center'
              }}>
              <View
                style={{
                  backgroundColor: 'red',
                  padding: 10,
                  borderRadius: 100
                }}>
                <Text style={styles.price}>
                  Rp. {numbro(item.price).format({ thousandSeparated: true })}
                </Text>
              </View>
            </View>
            <Text style={styles.description}>{item.description}</Text>
            <View
              style={{
                borderBottomColor: '#e8e8e8',
                borderBottomWidth: 1,
                marginVertical: 15,
                marginHorizontal: 10
              }}
            />
            <View>
              <Text style={styles.titleReview}>Ulasan Pembeli</Text>

              {review.review && review.review.length > 0 ? (
                review.review.map((_item, i) => {
                  return (
                    <View style={styles.reviewContainer} key={i}>
                      <View
                        style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Ionicons
                          name="star"
                          color={'#ffb300'}
                          size={18}
                          style={{ paddingRight: 5 }}
                        />
                        <Text style={{ fontWeight: 'bold', fontSize: 18 }}>
                          {_item.rating}
                        </Text>
                      </View>
                      <View
                        style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Text>oleh : </Text>
                        <Text style={{ fontWeight: 'bold', color: '#F18F40' }}>
                          {_item.name}
                        </Text>
                      </View>
                      <Text style={{ marginTop: 10, fontWeight: 'bold' }}>
                        {_item.review}
                      </Text>
                    </View>
                  );
                })
              ) : (
                <>
                  <View
                    style={{
                      marginVertical: 30,
                      justifyContent: 'center',
                      alignItems: 'center'
                    }}>
                    <Image
                      style={{ width: 70, height: 70 }}
                      source={require('../../../assets/notFound.png')}
                    />
                    <Text
                      style={{
                        marginLeft: 10,
                        marginTop: 20,
                        color: 'grey',
                        fontSize: 16
                      }}>
                      belum ada ulasan :(
                    </Text>
                  </View>
                </>
              )}
            </View>
            <View
              style={{
                borderBottomColor: '#e8e8e8',
                borderBottomWidth: 1,
                marginVertical: 15,
                marginHorizontal: 10
              }}
            />
            <View>
              <Text style={styles.titleReview}>Produk Terkait</Text>
              <View>
                <ProdukTerkait data={product} />
              </View>
            </View>
          </ScrollView>
        </View>

        <View style={styles.containerBottom}>
          {item.productCategoryId &&
          item.productCategoryId >= 2 &&
          item.productCategoryId <= 3 ? (
              <TouchableOpacity
                onPress={
                  () => dispatch(inputMsisdn(item))
                }>
                <View style={styles.priceWrap2}>
                  <Text style={styles.textBottom}>Pilih Nomor</Text>
                </View>
              </TouchableOpacity>
            ) : (
              <TouchableOpacity onPress={() => confirm()}>
                <View style={styles.priceWrap2}>
                  <Text style={styles.textBottom}>Masukkan Keranjang</Text>
                </View>
              </TouchableOpacity>
            )}

          <View>
            <TouchableOpacity onPress={() => RootNavigation.navigate('Cart')}>
              <CartIcon />
            </TouchableOpacity>
          </View>
        </View>
      </LinearGradient>
    </>
  );
}
