
import {
  StyleSheet,
  Dimensions
} from 'react-native';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({

  item: {
    padding: 10,
    marginVertical: 8,
    marginHorizontal: 16,
    borderRadius: 100
  },
  title: {
    fontSize: 14,
    fontWeight: 'bold',
    margin: 5
  },
  GantiHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginLeft: 0.0555 * width,
    marginRight: 0.0555 * width,
    marginTop: 0.0612 * height,
    marginBottom: 0.0212 * height
  },
  fontGanti: {
    fontSize: 20,
    color: 'black',
    alignSelf: 'center',
    marginLeft: 0.0942 * width,
    fontWeight: 'bold'
  },
  poster: {
    height: 0.95 * width,
    width: 0.95 * width,
    margin: 5,
    alignItems: 'center',
    borderRadius: 20
  },
  name: {
    fontSize: 18,
    fontWeight: 'bold',
    marginLeft: 10,
    marginBottom: 5,
    textAlign: 'center'
  },
  subProduct: {
    fontWeight: 'bold',
    marginTop: 5,
    marginLeft: 20,
    marginBottom: 10,
    fontSize: 14,
    color: '#F18F40'
  },
  price: {
    fontSize: 20,
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
    alignItems: 'center'
  },
  priceWrap: {
    margin: 10,
    backgroundColor: 'red',
    borderRadius: 200,
    justifyContent: 'center',
    padding: 5
  },
  description: {
    fontSize: 16,
    marginHorizontal: 20,
    marginBottom: 5,
    marginVertical: 30
  },
  container: {
    backgroundColor: '#FEFAF5',
    height: 0.9 * height,
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 30,
    padding: 5
  },
  containerBottom: {
    height: 0.1 * height,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  textBottom: {
    color: 'red',
    fontSize: 16,
    margin: 5,
    fontWeight: 'bold'
  },

  priceWrap2: {
    backgroundColor: 'white',
    borderRadius: 100,
    padding: 10,
    marginHorizontal: 30
  },

  liniearGradient: {
    width: width,
    height: height,
    justifyContent: 'space-between'
  },
  reviewContainer: {
    marginVertical: 15,
    marginLeft: 20
  },
  titleReview: {
    fontSize: 20,
    marginLeft: 20,
    fontWeight: 'bold'
  }
});
export default styles;
