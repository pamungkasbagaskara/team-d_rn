import React from 'react';

import CheckoutScreen from './CheckoutScreen';

const Checkout = () => {
  return (
    <CheckoutScreen />
  );
};

export default Checkout;
