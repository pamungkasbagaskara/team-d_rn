import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import React, { useEffect, useState } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  Alert
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import BottomSheet from 'reanimated-bottom-sheet';
import { TextInput } from 'react-native-gesture-handler';
import { useSelector, useDispatch } from 'react-redux';
import Config from 'react-native-config';

import * as RootNavigation from '../../navigation/RootNavigation';
import { BackIcon, CartIcon, NextIcon } from '../../components/Icon';
import { addCart } from '../../store/actions/productAction';

import styles from './MsisdnStyle';

export default function MsisdnScreen() {
  const { height } = Dimensions.get('window');
  const [data, setData] = useState([]);
  const [digit, setDigit] = useState();
  const itm = useSelector((state) => state.productStore.msisdn);
  const dispatch = useDispatch();

  const confirm = (id, nbr) => {
    Alert.alert(
      `${nbr}`,
      'Apakah Kamu Memilih Nomor ini?',
      [
        {
          text: 'Pilih Lainnya',
          style: 'cancel'
        },
        { text: 'Ya Bungkus', onPress: () => dispatch(addCart(id, nbr)) }
      ],
      { cancelable: false },
    );
  };

  const renderContent = () => (
    <View style={styles.panel}>
      <View style={styles.panelTitle}>
        {itm.subProduct === 'Simpati' ||
        itm.subProduct === 'Loop' ||
        itm.subProduct === 'KartuAs' ? (
            <>
              <Text style={styles.fontSheet}>Pilih Nomor Pilihan Anda</Text>
            </>
          ) : (
            <>
              <Text style={styles.fontSheet}>
              Pilih Digit Sesukamu (Max 4 Digit) :
              </Text>

              <View
                style={{ alignItems: 'center', justifyContent: 'space-between' }}>
                <View style={styles.input}>
                  <TextInput
                    placeholder="_ _ _ _"
                    maxLength={4}
                    style={{
                      borderRadius: 10,
                      fontSize: 30,
                      paddingVertical: 10,
                      paddingLeft: 30,
                      width: 170,
                      fontWeight: 'bold'
                    }}
                    value={digit}
                    keyboardType={'numeric'}
                    onChangeText={(text) => setDigit(text)}
                  />
                  <TouchableOpacity onPress={() => getData()} title="Lihat Nomor">
                    <NextIcon />
                  </TouchableOpacity>
                </View>
              </View>
            </>
          )}

        {data.result &&
          data.result.map((item, i) => {
            return (
              <TouchableOpacity
                onPress={
                  () => confirm(itm.id,item.number)
                }
                key={i}>
                <View style={styles.msisdn} key={i}>
                  <Text style={styles.msisdnNumber} key={i}>
                    {item.number}{' '}
                  </Text>
                </View>
              </TouchableOpacity>
            );
          })}

        {/* :(
         <View><Text>halooo ini pasca</Text></View>
        ) */}
      </View>
    </View>
  );

  const renderHeader = () => (
    <View style={styles.header}>
      <View style={styles.panelHeader}>
        <View style={styles.panelHandle} />
      </View>
    </View>
  );

  const getData = async () => {

    const item = await AsyncStorage.getItem('token');
    if (item !== null) {
      if (
        itm.subProduct === 'Simpati' ||
        itm.subProduct === 'Loop' ||
        itm.subProduct === 'KartuAs'
      ) {
        await axios
          .get(`${Config.BASE_URL}product/number`, {
            headers: {
              'Content-Type': 'application/json',
              Authorization: `Bearer ${item}`
            },
            params: {
              productCategory: 'starterpack',
              subProduct: `${itm.subProduct}`
            }
          })
          .then(function (response) {
            setData(response.data);
          })
          .catch(function (error) {
            Alert.alert(error);
          });
      } else {
        if (digit !== '') {
          await axios
            .get(`${Config.BASE_URL}product/number`, {
              headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${item}`
              },
              params: {
                productCategory: 'pascabayar',
                preferNumber: `${digit}`
              }
            })
            .then(function (response) {
              setData(response.data);
            })
            .catch(function (error) {
            });
        } else {
          Alert.alert('masukkan minimal 1 digit');
        }
      }
    }
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <LinearGradient
      start={{ x: 0, y: 0 }}
      end={{ x: 1, y: 0 }}
      colors={['#e52d27', '#b31217']}
      style={styles.container}>
      <View style={styles.homeHeader}>
        <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
          <TouchableOpacity onPress={() => RootNavigation.pop()}>
            <BackIcon />
          </TouchableOpacity>
          <Text style={styles.fontHalo}>Pilih Nomor</Text>
        </View>
        <View>
          <TouchableOpacity onPress={() => RootNavigation.navigate('Cart')}>
            <CartIcon />
          </TouchableOpacity>
        </View>
      </View>
      <BottomSheet
        snapPoints={[height * 0.85, height * 0.5, height * 0.2]}
        renderContent={renderContent}
        renderHeader={renderHeader}
        initialSnap={0}
      />
    </LinearGradient>
  );
}
