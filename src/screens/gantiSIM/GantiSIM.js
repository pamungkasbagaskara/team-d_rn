/* eslint-disable eqeqeq */
import React, { useState, useEffect } from 'react';
import {
  Text,
  TouchableOpacity,
  View,
  Dimensions,
  Image,
  TextInput,
  Alert
} from 'react-native';
import BottomSheet from 'reanimated-bottom-sheet';
import Ionicons from 'react-native-vector-icons/Ionicons';
import LinearGradient from 'react-native-linear-gradient';
import { useSelector, useDispatch } from 'react-redux';

import { CartIcon, BackIcon } from '../../components/Icon';
import { getDataProfile, goUpdateThenGantiSIM } from '../../store/actions/profileAction';
import { changeSIMAction } from '../../store/actions/productAction';
import { getOrders } from '../../store/actions/orderAction';

import styles from './GantiSIM-style';

const GantiSIM = ({ navigation }) => {
  const sheetRef = React.useRef(null);
  const dispatch = useDispatch();
  const { height } = Dimensions.get('window');
  const [product_id] = useState(24);
  const [qty] = useState(1);
  const [number, setNumber] = useState();
  const [ktp, setKtp] = useState();
  const [kk, setKk] = useState();
  const profileKK = useSelector((state) => state.profileStore.profile.kk);
  const profileKTP = useSelector((state) => state.profileStore.profile.ktp);
  const profilePhone = useSelector((state) => state.profileStore.profile.phones);
  const order = useSelector((state) => state.orderStore.order);
  const isUpdatingOrder = useSelector((state) => state.orderStore.isUpdatingOrder);
  const isUpdatingProfile = useSelector((state) => state.profileStore.isUpdatingProfile);

  useEffect(() => {
    dispatch(getOrders());
  }, []
  );
  useEffect(() => {
    dispatch(getOrders());
  }, [isUpdatingOrder]
  );
  useEffect(() => {
    dispatch(getDataProfile());
  }, [isUpdatingProfile]
  );

  const validateRegex = () => {
    var patternHP = new RegExp(/^(\\+62|\\+0|0|62)8(1[123]|52|53|21|22|23)[0-9]{5,9}$/);
    var patternKTP = new RegExp(/^((1[1-9])|(21)|([37][1-6])|(5[1-4])|(6[1-5])|([8-9][1-2]))[0-9]{2}[0-9]{2}(([0-6][0-9])|(7[0-1]))((0[1-9])|(1[0-2]))([0-9]{2})[0-9]{4}$/);
    var patternKK = new RegExp(/\d{16}$/);
    if (patternHP.test(number)) {
      if (patternKTP.test(ktp)){
        if (patternKK.test(kk)){
          handleSIM(product_id, qty, number, ktp, kk, profilePhone);
        } else {
          Alert.alert('Peringatan','Silahkan masukan nomor kk yang valid');
        }
      } else {
        Alert.alert('Peringatan','Silahkan masukan nomor ktp yang valid');
      }
    } else {
      Alert.alert('Peringatan','Silahkan masukan nomor telkomsel yang valid');
    }
  };
  const handleSIM = async () => {
    let bodyProfile = { ktp, kk, phones: profilePhone };
    let bodyChangeSIM = { product_id, qty, number, ktp, kk };
    if (order && order.product ) {
      if (order.product.filter((value) => value.number == number).length > 0){
        Alert.alert('Peringatan',`Transaksi Ganti Kartu SIM dengan nomor ${number} sudah ada di dalam keranjang`);
      } else {
        if (profileKK && profileKTP) {
          if (profilePhone.filter((value) => value.phone == number).length > 0) {
            dispatch(changeSIMAction(bodyChangeSIM));
          } else {
            if (profilePhone.length < 3) {
              profilePhone.push({ phone: number });
              let addBodyProfile = { phones: profilePhone };
              dispatch(goUpdateThenGantiSIM(addBodyProfile, bodyChangeSIM));
            } else {
              Alert.alert('Peringatan','Nomor sudah Maksimal 3');
            }
          }
        } else {
          if (profilePhone.filter((value) => value.phone == number).length > 0) {
            dispatch(goUpdateThenGantiSIM(bodyProfile, bodyChangeSIM));
          } else {
            if (profilePhone.length < 3) {
              let addPhone = profilePhone.push({ phone: number });
              let addBodyProfile = { ktp,kk, phones: addPhone };
              dispatch(goUpdateThenGantiSIM(addBodyProfile, bodyChangeSIM));
            } else {
              Alert.alert('Peringatan','Nomor tidak sesuai dengan database');
            }
          }
        }
      }
    } else {
      if (profileKK && profileKTP) {
        if (profilePhone.filter((value) => value.phone == number).length > 0) {
          dispatch(changeSIMAction(bodyChangeSIM));
        } else {
          if (profilePhone.length < 3) {
            profilePhone.push({ phone: number });
            let addBodyProfile = { phones: profilePhone };
            dispatch(goUpdateThenGantiSIM(addBodyProfile, bodyChangeSIM));
          } else {
            Alert.alert('Peringatan','Nomor sudah Maksimal 3');
          }
        }
      } else {
        if (profilePhone.filter((value) => value.phone == number).length > 0) {
          dispatch(goUpdateThenGantiSIM(bodyProfile, bodyChangeSIM));
        } else {
          if (profilePhone.length < 3) {
            let addPhone = profilePhone.push({ phone: number });
            let addBodyProfile = { ktp,kk, phones: addPhone };
            dispatch(goUpdateThenGantiSIM(addBodyProfile, bodyChangeSIM));
          } else {
            Alert.alert('Peringatan','Nomor tidak sesuai dengan database');
          }
        }
      }
    }
  };


  const renderContent = () => (
    <View style={styles.panel}>
      <View style={styles.panelTitle}>
        <Text style={styles.fontSheet}>Masukkan Data Diri </Text>
      </View>
      <View style={styles.inputContainer}>
        <View style={styles.inputView}>
          <Ionicons
            name="call"
            size={24}
            color={'red'}
            style={styles.iconInput}
          />
          <TextInput
            placeholder="Nomor Handphone"
            keyboardType="number-pad"
            autoCapitalize="none"
            placeholderTextColor="#B4ACAC"
            style={styles.textInput}
            onChangeText={(val) => setNumber(val)}
          />
        </View>
        <View style={styles.inputView}>
          <Ionicons
            name="card-outline"
            size={24}
            color={'red'}
            style={styles.iconInput}
          />
          <TextInput
            placeholder="Nomor KTP"
            keyboardType="number-pad"
            autoCapitalize="none"
            placeholderTextColor="#B4ACAC"
            style={styles.textInput}
            onChangeText={(val) => setKtp(val)}
          />
        </View>
        <View style={styles.inputView}>
          <Ionicons
            name="card-outline"
            size={24}
            color={'red'}
            style={styles.iconInput}
          />
          <TextInput
            placeholder="Nomor KK"
            keyboardType="number-pad"
            autoCapitalize="none"
            placeholderTextColor="#B4ACAC"
            style={styles.textInput}
            onChangeText={(val) => setKk(val)}
          />
        </View>
      </View>
      <View style={styles.bottomContainer}>
        <TouchableOpacity
          style={styles.buttonSubmit}
          onPress={() =>
            validateRegex(product_id, qty, number, ktp, kk, profilePhone)
          }
        >
          <Text style={styles.fontSubmit}>Submit</Text>
        </TouchableOpacity>
      </View>
    </View>
  );

  const renderHeader = () => (
    <View style={styles.header}>
      <View style={styles.panelHeader}>
        <View style={styles.panelHandle} />
      </View>
    </View>
  );
  return (
    <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['#e52d27', '#b31217']} style={styles.container}>
      <View style={styles.GantiHeader}>
        <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
          <TouchableOpacity onPress={() => navigation.navigate('Home')}>
            <BackIcon />
          </TouchableOpacity>
          <Text style={styles.fontGanti}>Ganti Sim Card</Text>
        </View>
        <View>
          <TouchableOpacity onPress={() => navigation.navigate('Cart')}>
            <CartIcon />
          </TouchableOpacity>
        </View>
      </View>
      <Image
        style={styles.gantiSimImage}
        source={require('../../../assets/gantiSim.png')}
      />
      <BottomSheet
        ref={sheetRef}
        snapPoints={[0.8 * height, 0.6 * height, 0.4 * height]}
        renderContent={renderContent}
        renderHeader={renderHeader}
        initialSnap={0}
      />
    </LinearGradient>
  );
};

export default GantiSIM;
