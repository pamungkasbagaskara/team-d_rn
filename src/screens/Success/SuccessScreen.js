import React, { useEffect } from 'react';
import {
  Text,
  View,
  TouchableOpacity } from 'react-native';
import { useDispatch } from 'react-redux';
import LottieView from 'lottie-react-native';
import LinearGradient from 'react-native-linear-gradient';

import * as RootNavigation from '../../navigation/RootNavigation';
import { getOrders } from '../../store/actions/orderAction';

import styles from './SuccessStyle';

const SuccessScreen = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getOrders());
  }, []);

  return (
    <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['#e52d27', '#b31217']} style={styles.container}>
      <View style={styles.titleBar}>
        <Text style={styles.title}>Sukses</Text>
      </View>
      <LottieView source={require('./../../asset/lottiefiles/success.json')} autoPlay loop />
      <Text style={styles.subtitle}>Kami segera mengirimkan barang pesanan kamu</Text>
      <TouchableOpacity onPress={()=>RootNavigation.popToTop()}>
        <View style={styles.buttonContainer}>
          <Text style={styles.buttonTitle}>Kembali ke Home</Text>
        </View>
      </TouchableOpacity>
    </LinearGradient>
  );
};

export default SuccessScreen;
