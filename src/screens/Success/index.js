import React from 'react';

import SuccessScreen from './SuccessScreen';

const Success = () => {
  return (
    <SuccessScreen />
  );
};

export default Success;
