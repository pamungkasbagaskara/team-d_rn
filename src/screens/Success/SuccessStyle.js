import { Dimensions, StyleSheet } from 'react-native';

const { height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#DE3030',
    alignItems: 'center'
  },
  title: {
    marginTop: height * 0.15,
    fontSize: 35,
    fontWeight: 'bold',
    color: '#FFF',
    width: '75%',
    textAlign: 'center'
  },
  subtitle: {
    fontSize: 17,
    fontWeight: 'bold',
    color: '#FFF',
    width: '75%',
    textAlign: 'center',
    marginTop: height * 0.55
  },
  label: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#F18F40',
    marginVertical: 5
  },
  buttonContainer: { backgroundColor: 'white', margin: 30, padding: 10, paddingHorizontal: 20, borderRadius: 100 },
  buttonTitle: { color: 'red', fontWeight: 'bold' }
});
export default styles;
