import React, { useState, useRef, useEffect } from 'react';
import {
  Text,
  FlatList,
  View,
  Image,
  TouchableOpacity,
  Dimensions
} from 'react-native';

import BottomSheet from 'reanimated-bottom-sheet';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { useSelector, useDispatch } from 'react-redux';
import NumberFormat from 'react-number-format';
import LinearGradient from 'react-native-linear-gradient';
import * as Animatable from 'react-native-animatable';

import ButtonSheet from '../../components/ButtonBS';
import * as RootNavigation from '../../navigation/RootNavigation';
import { getOrders } from '../../store/actions/orderAction';
import OrderList from '../../components/OrderList';

import styles from './CartStyle';

const { height } = Dimensions.get('window');

const CartScreen = () => {
  const panelRef = useRef();
  const buttonRef = useRef();
  const dispatch = useDispatch();
  const [buttonTransparency, setButtonTransparency] = useState(false);
  const order = useSelector((state) => state.orderStore.order);
  const isUpdatingOrder = useSelector((state) => state.orderStore.isUpdatingOrder);

  useEffect(() => {
    dispatch(getOrders());
  }, []);

  useEffect(() => {
    dispatch(getOrders());
  }, [isUpdatingOrder]);

  const buttonSheet = () => {
    if (order && order.totalItem > 0) {
      return (
        <NumberFormat
          value={order.totalPrice}
          displayType={'text'}
          prefix={'Rp. '}
          renderText={(value) => (
            <ButtonSheet
              style={buttonTransparency ? { opacity: 0.1 } : { opacity: 1 }}
              subtitle="Checkout"
              title={value}
              titleStyle={{ color: '#000' }}
              onPress={() => RootNavigation.navigate('Checkout')}
            />
          )}
          thousandSeparator={'.'}
          decimalSeparator={','}
        />
      );
    }
  };

  const renderContent = () => (
    <Animatable.View animation="slideInUp" style={styles.panel}>
      <View style={styles.panelTitle}>
        <TouchableOpacity onPress={() => RootNavigation.popToTop()}>
          <Ionicons
            name="chevron-back-circle"
            size={30}
            color={'#DE3030'}
            style={{ paddingRight: 10, opacity: 0.3 }}
          />
        </TouchableOpacity>
        <Text style={styles.title}>Keranjang Belanja</Text>
      </View>

      {Object.entries(order).length >= 1 ? (
        <View style={styles.panelPayment}>
          {order.totalItem > 0 ? <Text style={styles.label}>Total : {order.totalItem} Items</Text>
            : <Text style={styles.label}>Anda belum memesan apapun</Text>
          }
          <FlatList
            data={order.product}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item }) => <OrderList data={item} />}
            showsVerticalScrollIndicator={false}
            style={{ paddingBottom: 100 }}
            onScrollBeginDrag={() => setButtonTransparency(true)}
            onScrollEndDrag={() => setButtonTransparency(false)}
          />
        </View>
      ) : (
        <View style={styles.panelPayment}>
          <Text style={styles.label}>Anda belum memesan apapun</Text>
        </View>
      )}
    </Animatable.View>
  );

  const renderHeader = () => (
    <Animatable.View animation="slideInUp" style={styles.header}>
      <View style={styles.panelHeader}>
        <View style={styles.panelHandle} />
      </View>
    </Animatable.View>
  );

  return (
    <>
      <LinearGradient
        start={{ x: 0, y: 0 }}
        end={{ x: 1, y: 0 }}
        colors={['#e52d27', '#b31217']}
        style={styles.container}>
        <Image
          style={styles.cartImage}
          source={require('../../../assets/cart.png')}
        />
      </LinearGradient>

      <BottomSheet
        ref={panelRef}
        snapPoints={[height * 0.85, height * 0.7, height * 0.5]}
        renderContent={renderContent}
        renderHeader={renderHeader}
        initialSnap={0}
        enabledInnerScrolling={true}
        enabledContentGestureInteraction={false}
      />

      {Object.entries(order).length >= 1 && (
        <BottomSheet
          ref={buttonRef}
          snapPoints={[height * 0.15]}
          renderContent={buttonSheet}
          initialSnap={0}
        />
      )}
    </>
  );
};

export default CartScreen;
