import { Dimensions, StyleSheet } from 'react-native';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#DE3030',
    alignItems: 'center'
  },
  panel: {
    marginTop: -10,
    height: height * 0.925,
    paddingVertical: 20,
    paddingBottom: height * 0.1,
    paddingHorizontal: 20,
    backgroundColor: '#FEFAF5'
  },
  cartImage: {
    width: 1 * width,
    height: 0.5 * height,
    alignSelf: 'center',
    marginBottom: 10
  },
  header: {
    backgroundColor: '#FEFAF5',
    shadowColor: '#000000',
    paddingTop: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20
  },
  panelHeader: {
    alignItems: 'center'
  },
  panelHandle: {
    width: 100,
    height: 8,
    borderRadius: 4,
    backgroundColor: '#F18F40',
    marginBottom: 5
  },
  panelTitle: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  title: {
    fontSize: width * 0.075,
    fontWeight: 'bold',
    color: '#DE3030',
    width: '75%',
    textAlign: 'center'
  },
  panelPrice: {
    flex: 1.5,
    paddingVertical: 10
  },
  label: {
    fontSize: width * 0.05,
    fontWeight: 'bold',
    color: '#F18F40',
    marginVertical: 5
  },
  price: {
    fontWeight: 'bold',
    fontSize: 30,
    color: '#262626'
  },
  panelPayment: {
    flex: 9,
    paddingVertical: 10
  }
});
export default styles;
