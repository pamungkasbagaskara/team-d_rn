import { StyleSheet, Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    width: width,
    height: height,
    flex: 1
  },
  image: {
    marginTop: height * 0.14,
    width: width,
    height: width
  },
  logo: {
    width: width * 0.15,
    height: width * 0.15,
    borderRadius: 100,
    backgroundColor: '#E4A0A0',
    borderColor: '#E4A0A0',
    backfaceVisibility: 'visible',
    alignSelf: 'center',
    shadowColor: '#E4A0A0',
    shadowOffset: { width: 0.5, height: 0.5 },
    shadowOpacity: 0.5
  },
  home: {
    alignItems: 'center',
    marginTop: height * 0.1,
    justifyContent: 'center',
    alignContent: 'center',
    fontSize: 16,
    color: 'red'
  },

  userInfoSection: {
    padding: height * 0.05,
    alignSelf: 'center'
  },
  title: {
    fontSize: 30,
    fontWeight: 'bold',
    color: '#F2F2F2',
    justifyContent: 'center',
    alignSelf: 'center',
    paddingTop: height * 0.02,
    width: width
  },

  title_input: {
    width: width,
    height: height * 0.08,
    color: '#000000',
    fontWeight: 'bold',
    fontSize: 24,
    fontFamily: 'arial',
    top: height * 0.08,
    alignSelf: 'center',
    textAlign: 'center'
  },
  body_text: {
    fontWeight: 'bold',
    fontSize: 18,
    padding: height * 0.01,
    top: height * 0.02,
    left: width * 0.3,
    fontFamily: 'arial',
    alignSelf: 'flex-start',
    justifyContent: 'flex-start'
  },
  child_text: {
    fontWeight: 'bold',
    fontSize: 10,
    padding: height * 0.01,
    top: height * 0.025,
    left: width * 0.26,
    fontFamily: 'arial',
    alignSelf: 'flex-start',
    justifyContent: 'flex-start'
  },
  icon: {
    position: 'absolute',
    left: width * 0.2,
    top: height * 0.03
  },
  edit: {
    top: height * 0.08,
    right: width * 0.05,
    alignSelf: 'flex-end',
    textAlign: 'center'
  },
  edits: {
    position: 'absolute',
    right: width * 0.1,
    alignSelf: 'flex-start'
  },
  body_subtext: {
    top: height * 0.015,
    fontWeight: 'normal',
    fontSize: 15,
    left: width * 0.32,
    padding: height * 0.0005,
    fontFamily: 'arial',
    alignSelf: 'flex-start'
  },
  dataInput: {
    width: width,
    height: height * 0.28,
    color: '#000000',
    fontWeight: 'bold',
    fontSize: 24,
    fontFamily: 'arial',
    padding: height * 0.002,
    marginTop: height * 0.06,
    paddingVertical: height * 0.02
  },
  dataFooter: {
    width: width,
    color: '#000000',
    fontWeight: 'bold',
    fontSize: 14,
    fontFamily: 'arial',
    marginTop: height * 0.05,
    alignSelf: 'center',
    alignContent: 'center',
    height: height * 0.15,
    textAlign: 'center'
  },
  contentContainer: {
    width: width,
    height: height * 0.1,
    marginTop: height * 0.153,
    backgroundColor: '#000',
    borderBottomLeftRadius: height * 0.05,
    borderBottomRightRadius: height * 0.05
  },
  text_footer: {
    color: '#828282',
    alignSelf: 'center',
    fontSize: 16,
    paddingTop: 20
  },
  action: {
    flexDirection: 'row',
    marginTop: 10,
    paddingLeft: width * 0.15,
    paddingBottom: width * 0.05,
    alignSelf: 'flex-start'
  },
  textInput: {
    maxWidth: 270,
    color: '#828282',
    flex: 1,
    paddingRight: width * 0.05,
    textAlign: 'right',
    justifyContent: 'space-evenly'
  },

  buttons: {
    alignItems: 'center',
    width: width * 0.5,
    height: width * 0.12,
    justifyContent: 'center',
    alignContent: 'center'
  },
  button: {
    alignItems: 'flex-end',
    width: width * 0.5,
    height: width * 0.12,
    marginTop: height * 0.01,
    marginLeft: width * 0.05,
    shadowColor: '#de3030',
    shadowOffset: { width: 0.5, height: 0.5 },
    shadowOpacity: 0.5,
    fontWeight: 'bold'
  },
  button_home: {
    width: width * 0.2,
    height: width * 0.13,
    marginTop: height * 0.005,
    shadowColor: '#de3030',
    backgroundColor: 'white',
    shadowOffset: { width: 0.5, height: 0.5 },
    shadowOpacity: 0.5,
    paddingVertical: height * 0.01,
    paddingLeft: width * 0.06,
    borderTopRightRadius: 50,
    borderBottomRightRadius: 50
  },
  button_logout: {
    width: width * 0.2,
    height: width * 0.13,
    marginTop: height * 0.01,
    marginLeft: width * 0.05,
    shadowColor: '#de3030',
    backgroundColor: 'white',
    shadowOffset: { width: 0.5, height: 0.5 },
    shadowOpacity: 0.5,
    paddingVertical: height * 0.01,
    paddingLeft: width * 0.08,
    borderTopLeftRadius: 50,
    borderBottomLeftRadius: 50,
    alignContent: 'flex-start'
  },
  data: {
    fontSize: 13,
    fontWeight: 'bold'
  },
  dataTrans: {
    width: width,
    height: height * 0.45,
    color: '#000000',
    fontWeight: 'bold',
    fontSize: 24,
    fontFamily: 'arial',
    alignItems: 'center',
    alignContent: 'center',
    padding: height * 0.002,
    marginTop: height * 0.06
  },
  containerTransHis: {
    backgroundColor: 'white',
    width: 0.85 * width,
    height: 0.2 * height,
    borderColor: '#F18F40',
    borderRadius: 20,
    borderWidth: 1,
    padding: 10,
    marginBottom: 10
  },
  imgcard: {
    width: 50,
    height: 50
  },
  fontID: {
    color: '#F18F40',
    fontWeight: 'bold',
    fontSize: 10
  },
  fontName: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: 15,
    width: 0.61545 * width
  },
  fontStatus: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 15
  },
  status: {
    backgroundColor: '#60CD3A',
    width: 0.351545 * width,
    borderRadius: 20,
    alignItems: 'center',
    position: 'absolute',
    marginLeft: 0.24 * width,
    marginTop: 0.0802 * height
  },
  statusWaiting: {
    backgroundColor: '#CD413A',
    width: 0.351545 * width,
    borderRadius: 20,
    alignItems: 'center',
    position: 'absolute',
    marginLeft: 0.24 * width,
    marginTop: 0.0802 * height
  },
  fontOthers: {
    color: 'grey',
    fontSize: 12
  },
  fontTitle: {
    fontWeight: 'bold',
    fontSize: 18,
    marginBottom: 10
  },
  containerKanan: {
    marginLeft: 10,
    justifyContent: 'space-around',
    height: 0.12 * height,
    width: 0.5745 * width
  },
  fontTotal: {
    color: 'black',
    fontSize: 12
  },
  hitSlop: { top: width * 0.05, bottom: width * 0.05, left: width * 0.08, right: width * 0.08 }
});

export default styles;
