import React, { useEffect } from 'react';
import LottieView from 'lottie-react-native';
import LinearGradient from 'react-native-linear-gradient';
import { useDispatch } from 'react-redux';

import { getDataProfile } from '../../store/actions/profileAction';



const Splash = ({}) => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getDataProfile());
  }, []);

  return (
    <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
      colors={['#e52d27', '#b31217']}
      style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
      <LottieView source={require('../../../assets/splash.json')}
        autoPlay
        loop = {false}
        speed = {0.5} />
    </LinearGradient>
  );
};

export default Splash;


