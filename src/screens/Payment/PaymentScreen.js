import React, { useState, useRef, useEffect } from 'react';
import {
  Text,
  FlatList,
  View,
  TouchableOpacity,
  Dimensions,
  Alert
} from 'react-native';

import BottomSheet from 'reanimated-bottom-sheet';
import Ionicons from 'react-native-vector-icons/Ionicons';
import NumberFormat from 'react-number-format';
import { useSelector, useDispatch } from 'react-redux';
import LinearGradient from 'react-native-linear-gradient';
import * as Animatable from 'react-native-animatable';
import LottieView from 'lottie-react-native';
import Spinner from 'react-native-loading-spinner-overlay';

import * as RootNavigation from '../../navigation/RootNavigation';
import ButtonSheet from '../../components/ButtonBS';
import { getListBank, selectBank } from '../../store/actions/bankAction';
import Bank from '../../components/Bank';
import { pay, virtualPay } from '../../store/actions/paymentAction';

import styles from './PaymentStyle';

const { height } = Dimensions.get('window');

const PaymentScreen = () => {
  const panelRef = useRef();
  const buttonRef = useRef();
  const dispatch = useDispatch();
  const [buttonTransparency, setButtonTransparency] = useState(false);
  const bank = useSelector((state) => state.bankStore.bank);
  const transactionId = useSelector((state) => state.orderStore.current_transaction);
  const totalPrice = useSelector((state) => state.orderStore.total_transaction);
  const isLoading = useSelector((state) => state.paymentStore.isLoading);
  const selectedBank = useSelector((state) => state.bankStore.selectedBank);
  const isPayWithVirtualBank = useSelector((state) => state.bankStore.isPayWithVirtualBank);
  const virtualBank = useSelector((state) => state.bankStore.virtualBank);

  useEffect(() => {
    dispatch(getListBank());
  }, []);

  const PaymentHandler = () => {
    if (selectedBank) {
      if (isPayWithVirtualBank) {
        dispatch(virtualPay(transactionId, totalPrice, virtualBank));
      }
      else {
        dispatch(pay(transactionId, totalPrice, selectedBank));
      }
    }
    else {
      Alert.alert('Data belum Lengkap', 'Silahkan pilih metode pembayaran anda');
    }
  };

  const buttonSheet = () => (<ButtonSheet title="Bayar Sekarang" onPress={PaymentHandler} style={buttonTransparency ? { opacity: 0.1 } : { opacity: 1 }} />);

  const renderContent = () => (
    <Animatable.View animation="slideInUp" style={styles.panel}>
      <Spinner visible={isLoading} textStyle={styles.spinnerTextStyle} />
      <View style={styles.panelTitle}>
        <TouchableOpacity onPress={()=>RootNavigation.pop()}>
          <Ionicons name="chevron-back-circle" size={30} color={'#DE3030'} style={{ paddingRight: 10, opacity: 0.3 }} />
        </TouchableOpacity>
        <Text style={styles.title}>Pembayaran</Text>
      </View>

      <View style={styles.panelPrice}>
        <Text style={styles.label}>Total Pembayaran</Text>
        <NumberFormat value={totalPrice}
          displayType={'text'}
          prefix={'Rp. '}
          renderText={value => <Text style={styles.price}>{value}</Text>}
          thousandSeparator={'.'} decimalSeparator={','}
        />
      </View>

      <View style={styles.panelPayment}>
        <Text style={styles.label}>Pilih Metode Pembayaran</Text>
        <FlatList data={bank}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item }) => <Bank data={item} onPress={()=>dispatch(selectBank(item))} />}
          showsVerticalScrollIndicator={false}
          onScrollBeginDrag={() => setButtonTransparency(true)}
          onScrollEndDrag={() => setButtonTransparency(false)}
        />
      </View>
    </Animatable.View>
  );

  const renderHeader = () => (
    <Animatable.View animation="slideInUp" style={styles.header}>
      <View style={styles.panelHeader}>
        <View style={styles.panelHandle} />
      </View>
    </Animatable.View>
  );

  return (
    <>
      <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['#e52d27', '#b31217']} style={styles.container} >
        <LottieView source={require('./../../asset/lottiefiles/financial-transactions.json')} autoPlay loop />
      </LinearGradient>

      <BottomSheet
        ref={panelRef}
        snapPoints={[height * 0.9, height * 0.5, height * 0.2]}
        renderContent={renderContent}
        renderHeader={renderHeader}
        initialSnap={0}
        enabledInnerScrolling={true}
        enabledContentGestureInteraction={false}
      />

      <BottomSheet
        ref={buttonRef}
        snapPoints={[height * 0.15]}
        renderContent={buttonSheet}
        initialSnap={0}
      />
    </>
  );
};

export default PaymentScreen;
