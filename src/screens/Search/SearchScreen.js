import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, Image, Dimensions } from 'react-native';
import BottomSheet from 'reanimated-bottom-sheet';
import Ionicons from 'react-native-vector-icons/Ionicons';
import LinearGradient from 'react-native-linear-gradient';
import numbro from 'numbro';
import { useDispatch, useSelector } from 'react-redux';
import { ScrollView, TextInput } from 'react-native-gesture-handler';

import { getDataProduct, inputDetilProduct } from '../../store/actions/productAction';
import * as RootNavigation from '../../navigation/RootNavigation';
import { CartIcon, BackIcon } from '../../components/Icon';

import styles from './SearchStyle';

const { width, height } = Dimensions.get('window');

export default function SearchScreen() {
  const dispatch = useDispatch();
  // const [MainJSON, setMainJSON] = useState([]);
  const MainJSON = useSelector((state) => state.productStore.product.result);
  const [FilterData, setFilterData] = useState([]);
  const [value, onChangeText] = React.useState('');

  useEffect(() => {
    dispatch(getDataProduct());
  }, []);

  const renderHeader = () => (
    <View style={styles.header}>
      <View style={styles.panelHeader}>
        <View style={styles.panelHandle} />
      </View>
    </View>
  );

  const renderContent = () => (
    <View style={styles.panel}>
      <View style={styles.panelTitle}>
        <View style={styles.search}>
          <Ionicons name="search" size={18} color={'grey'} />
          <TextInput
            onChangeText={(text) => {
              onChangeText(text);
              SearchDataFromJSON(text);
            }}
            value={value}
            placeholder={'Cari Produk..'}
            style={{ width: width, marginLeft: 20 }}
          />
        </View>
        <ScrollView >
          {FilterData &&
            FilterData.map((item, i) => {
              return (
                <TouchableOpacity
                  onPress={() => {
                    dispatch(inputDetilProduct(item));
                  }}
                  key={i}>
                  <View style={{ margin: 10, flexDirection: 'row', width: 0.9 * width, borderRadius: 20, backgroundColor: 'white' }}>
                    <Image
                      style={{ width: 80, height: 80 }}
                      source={{ uri: item.poster }}
                    />
                    <View style={{ flexDirection: 'column', width: 0.7 * width }}>
                      <Text style={{ color: '#F18F40', marginLeft: 5 }}>
                        {item.subProduct}
                      </Text>
                      <Text style={styles.SearchBoxTextItem}>{item.name}</Text>
                      <Text style={styles.SearchBoxPriceItem}>
                        Rp.{' '}
                        {numbro(item.price).format({ thousandSeparated: true })}
                      </Text>
                    </View>
                  </View>
                </TouchableOpacity>
              );
            })}
        </ScrollView>
      </View>
    </View>
  );

  const SearchDataFromJSON = (_value) => {
    if (_value) {
      const regex = new RegExp(`${value.trim()}`, 'i');
      setFilterData(MainJSON.filter((data) => data.name.search(regex) >= 0));
    } else {
      setFilterData([]);
    }
  };

  return (
    <>
      <LinearGradient
        start={{ x: 0, y: 0 }}
        end={{ x: 1, y: 0 }}
        colors={['#e52d27', '#b31217']}
        style={styles.container}>
        <View style={styles.homeHeader}>
          <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
            <TouchableOpacity onPress={() => RootNavigation.pop()}>
              <BackIcon />
            </TouchableOpacity>
            <Text style={styles.fontHalo}>Cari Produk</Text>
          </View>
          <View>
            <TouchableOpacity onPress={() => RootNavigation.navigate('Cart')}>
              <CartIcon />
            </TouchableOpacity>
          </View>
        </View>
        <Image
          style={styles.homeImage}
          source={require('../../../assets/home.png')}
        />
        <BottomSheet
          snapPoints={[height * 0.85, height * 0.6, height * 0.4]}
          renderContent={renderContent}
          renderHeader={renderHeader}
          initialSnap={0}
        />
      </LinearGradient>
    </>
  );
}
