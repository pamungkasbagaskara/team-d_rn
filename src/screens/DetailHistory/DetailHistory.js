import React, { useState,useEffect } from 'react';
import { View, Text, Image,Dimensions,Modal,TextInput,TouchableOpacity,FlatList,Alert } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import NumberFormat from 'react-number-format';
import moment from 'moment';
import 'moment/locale/id';
import { useDispatch } from 'react-redux';
import { AirbnbRating } from 'react-native-ratings';

import { CartIcon, BackIcon } from '../../components/Icon';
import * as RootNavigation from '../../navigation/RootNavigation';
import { getListBank } from '../../store/actions/bankAction';
import { addReview } from '../../store/actions/reviewAction';

import styles from './DetailHistory-style';

const DetailHistory = ({ route,navigation }) => {
  const { width, height } = Dimensions.get('window');
  const { historyID } = route.params;
  const dispatch = useDispatch();
  const [rating, setRating] = useState();
  const [showModal, setShowModal] = useState(false);
  const [reviewDesc, setReviewDesc] = useState({});
  const [reviewProductID, setReviewProductID] = useState();
  const ratingCompleted = (_rating)=> {
    setRating(_rating);
  };
  moment.locale();

  useEffect(() => {
    dispatch(getListBank());
  }, []);
  const handleReview = async () => {
    let data = {
      'transactionId': `${reviewProductID}`,
      'review': `${reviewDesc}`,
      'rating': `${rating}`
    };
    if (rating && reviewDesc){
      dispatch(addReview(data));
      setReviewDesc();
      setRating();
      setShowModal(false);
    } else {
      Alert.alert('Peringatan', 'Lengkapi Rating dan deskripsi!');
    }

  };

  const renderItem = ({ item }) => {
    return (
      <View style={styles.containerTransHis}>
        <View style={{ flexDirection: 'row',height: 0.1102 * height }}>
          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Image
              style={styles.imgcard}
              source={{
                uri: `${item.poster}`
              }}
            />
          </View>

          <View style={styles.containerKanan}>
            <View>
              <Text style={styles.fontName}>{item.name}<Text style={{ fontWeight: 'normal' }}>({item.item} barang)</Text></Text>
              {item.number ? (
                <View>
                  <Text style={styles.fontTotal}>{item.number}</Text>
                </View>

              ) : (<View>
                <Text style={styles.fontTotal}> </Text>
              </View> )}
            </View>

            <View>
              <View
                style={{
                  borderBottomColor: 'grey',
                  borderBottomWidth: 1
                }}
              />
              <View style={{ flexDirection: 'row' }}>
                <View>
                  <Text style={styles.fontTotal}>Harga</Text>
                  <NumberFormat value={item.price}
                    displayType={'text'}
                    prefix={'Rp. '}
                    renderText={value => (
                      <Text style={styles.fontTotal}>{value} </Text>
                    )}
                    thousandSeparator={'.'} decimalSeparator={','}
                  />
                </View>
                {item.name === 'Ganti Kartu' ? (
                  <View />
                ) : (
                  <TouchableOpacity style={styles.review}
                    onPress={() => {
                      setReviewProductID(item.transactionId);
                      setShowModal(true);
                    }
                    }>
                    <Text>Tulis Review</Text>
                  </TouchableOpacity>
                )}
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  };



  return (
    <>
      <LinearGradient
        start={{ x: 0, y: 0 }}
        end={{ x: 1, y: 0 }}
        colors={['#e52d27', '#b31217']}
        style={styles.container}>

        <View style={styles.GantiHeader}>
          <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
            <TouchableOpacity onPress={()=>RootNavigation.pop()}>
              <BackIcon />
            </TouchableOpacity>
            <Text style={styles.fontGanti}>Detail transaksi</Text>
          </View>
          <View>
            <TouchableOpacity onPress={() => navigation.navigate('Cart')}>
              <CartIcon />
            </TouchableOpacity>
          </View>
        </View>
        <View>

          <View style={styles.containerProduk}>

            <View style={{ alignSelf: 'center', width: 0.9 * width }}>
              <FlatList
                ListHeaderComponent={
                  <View style={{ marginBottom: 10, paddingLeft: 15, paddingRight: 15 }}>
                    <View style={styles.containerStatus}>
                      <Text style={styles.fontStatus}>Status : </Text>
                      <Text style={styles.fontStatusItem}>{historyID.status}</Text>
                      <Text style={styles.fontStatus}>ID Transaksi : </Text>
                      <Text style={styles.fontStatusItem}>{historyID.id_transaction}</Text>
                      {historyID.payment && historyID.payment.paymentMethod !== null ? (
                        <View>
                          <Text style={styles.fontStatus}>Tanggal Transaksi :</Text>
                          <Text style={styles.fontStatusItem}>{moment(historyID.payment.paymentMethod.createdAt).format('Do MMMM YYYY, h:mm:ss a')}</Text>

                        </View>
                      ) : (
                        <View>
                          <Text style={styles.fontStatus}>Tanggal Transaksi :</Text>
                          <Text style={styles.fontStatusItem}>Belum Dilakukan Pembayaran!</Text>
                        </View>
                      )}
                    </View>
                    <View style={styles.containerPengiriman}>
                      <Text style={styles.fontJudul}>Detail Pengiriman</Text>
                      {historyID.address ? (
                        <View style={styles.containerAlamat}>
                          <View style={styles.alamatKiri}>
                            <Text style={styles.fontStatus2}>Alamat </Text>
                          </View>
                          <View style={styles.alamatKanan}>
                            <Text style={styles.fontAlamat}>{historyID.address.address_name}</Text>
                            <Text style={styles.fontAlamat}>{historyID.address.street}, Kelurahan {historyID.address.district}
                            , Kecamatan {historyID.address.regency}, {historyID.address.state}, Kode Pos {historyID.address.postal_code}</Text>


                          </View>
                        </View>
                      ) : (
                        <Text style={styles.fontStatusItem}>Belum dilakukan Pengiriman!</Text>
                      )}

                    </View>

                    <Text style={styles.fontJudul}>Produk</Text>


                  </View>

                }
                ListFooterComponent={
                  <View style={styles.containerPembayaran}>
                    <Text style={styles.fontJudul}>Informasi Pembayaran</Text>
                    {historyID.payment.paymentMethod ? (
                      <View style={styles.containerPayment}>
                        <View style={{ flexDirection: 'row',justifyContent: 'space-between' }}>
                          <Text style={styles.fontStatus2}>Metode Pembayaran </Text>
                          <Text style={styles.fontAlamat}>{historyID.payment.paymentMethod.bank}</Text>
                        </View>
                        <View style={{ borderBottomColor: 'grey',borderBottomWidth: 1, marginTop: 10 }}/>
                        <View style={{ flexDirection: 'row',justifyContent: 'space-between' }}>
                          <Text style={styles.fontStatus2}>Total Harga <Text style={{ fontWeight: 'normal', fontSize: 12 }}>
                            ({historyID.totalItem} barang)</Text></Text>
                          {/* {console.log('total paid', historyID.payment.total_paid)}
                            {console.log('total price', historyID.totalPrice)}
                            {console.log('total price', historyID.id_transaction)} */}
                          <NumberFormat value={historyID.totalPrice}
                            displayType={'text'}
                            prefix={'Rp. '}
                            renderText={value => (
                              <Text style={styles.fontAlamat}>{value} </Text>
                            )}
                            thousandSeparator={'.'} decimalSeparator={','}
                          />
                        </View>
                        <View style={{ flexDirection: 'row',justifyContent: 'space-between' }}>
                          <Text style={styles.fontStatus2}>Total Ongkos Kirim </Text>
                          <NumberFormat value={historyID.shippingCost}
                            displayType={'text'}
                            prefix={'Rp. '}
                            renderText={value => (
                              <Text style={styles.fontAlamat}>{value} </Text>
                            )}
                            thousandSeparator={'.'} decimalSeparator={','}
                          />
                        </View>
                        <View style={{ borderBottomColor: 'grey',borderBottomWidth: 1, marginTop: 10 }}/>
                        <View style={{ flexDirection: 'row',justifyContent: 'space-between' }}>
                          <Text style={styles.fontStatus2}>Total Bayar </Text>
                          <NumberFormat value={historyID.totalPaid}
                            displayType={'text'}
                            prefix={'Rp. '}
                            renderText={value => (
                              <Text style={styles.fontAlamat}>{value} </Text>
                            )}
                            thousandSeparator={'.'} decimalSeparator={','}
                          />
                        </View>
                      </View>
                    ) : (
                      <Text style={styles.fontStatusItem}>Belum dilakukan Pembayaran!</Text>
                    )}
                    <View style={{ height: 0.08 * height }} />
                  </View>

                }
                data = {historyID.products}
                renderItem={renderItem}
                showsVerticalScrollIndicator={false}
                keyExtractor={(item, index) => index.toString()}
              />


            </View>

          </View>
        </View>
        {/* ----------------MODAL ADD REVIEW------------------------------------------- */}
        <Modal transparent={true} visible={showModal} animationType="slide" >
          <View style={{ backgroundColor: '#000000aa', flex: 1, justifyContent: 'flex-end', alignContent: 'center' }}>
            <View style={styles.containerModal}>
              <Text style={styles.textReview}>
						Bagaimana pendapat anda tentang produk ini?</Text>
              <AirbnbRating
                count={10}
                showRating = {false}
                defaultRating={0}
                size={20}
                onFinishRating= {ratingCompleted}
              />

              <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 0.0109375 * height }} />
              <Text style={styles.textRating}>
            Rating Anda: {rating}
              </Text>
              <TextInput
                style={styles.inputDesc}
                placeholder="Tulis review anda di sini"
                placeholderTextColor="#979797"
                onChangeText={(value) => setReviewDesc(value)
                }
              />

              <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                <TouchableOpacity
                  style={styles.buttonCancel}
                  onPress={()=>
                    setShowModal(false)
                  }
                >
                  <Text style={styles.textCancel}>Cancel</Text>
                </TouchableOpacity>

                <TouchableOpacity style={styles.buttonSubmit} onPress={()=> handleReview()}>
                  <Text style={styles.textSubmit}>Submit</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>




      </LinearGradient>
    </>
  );
};

export default DetailHistory;
