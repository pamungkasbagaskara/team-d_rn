import React, { useState, useEffect } from 'react';
import {
  Text,
  TouchableOpacity,
  View,
  Dimensions,
  Image,
  Alert,
  Platform,
  BackHandler
} from 'react-native';
import BottomSheet from 'reanimated-bottom-sheet';
import Ionicons from 'react-native-vector-icons/Ionicons';
import NumberFormat from 'react-number-format';
import LinearGradient from 'react-native-linear-gradient';
import { useSelector, useDispatch } from 'react-redux';
import * as Animatable from 'react-native-animatable';

import { getDataProfile } from '../../store/actions/profileAction';
import { getDataPrice, navigateBundling, navigatePascabayar, navigateStarterpack } from '../../store/actions/productAction';
import { CartIcon, ProfileIcon } from '../../components/Icon';
import * as RootNavigation from '../../navigation/RootNavigation';
import SliderHome from '../../components/SliderHome';

import styles from './Homescreen-style';

const Homescreen = ({ navigation }) => {
  const sheetRef = React.useRef(null);
  const { height } = Dimensions.get('window');
  const [hargaStarterPack, setHargaStarterPack] = useState();
  const [hargaGantiKartu, setHargaGantiKartu] = useState();
  const [hargaPascabayar, setHargaPascabayar] = useState();
  const [hargaDeviceBundling, setHargaDeviceBundling] = useState();
  const dispatch = useDispatch();
  const profile = useSelector((state) => state.profileStore.profile);
  const price = useSelector((state) => state.productStore.price.result);
  const isUpdatingProfile = useSelector((state) => state.profileStore.isUpdatingProfile);

  useEffect(() => {
    dispatch(getDataProfile());
    dispatch(getDataPrice());
  }, []);
  useEffect(() => {
    handlePrice();
  }, [price]);
  useEffect(() => {
    dispatch(getDataProfile());
  }, [isUpdatingProfile]);

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', backAction);

    return () =>
      BackHandler.removeEventListener('hardwareBackPress', backAction);
  }, []);

  const backAction = () => {
    if (!navigation.canGoBack()) {
      Alert.alert('Tunggu sebentar!', 'Apakah anda yakin akan keluar?', [
        {
          text: 'Cancel',
          onPress: () => null,
          style: 'cancel'
        },
        { text: 'YES', onPress: () => BackHandler.exitApp() }
      ]);
      return true;
    } else {
      return false;
    }
  };
  const handlePrice = () => {
    if (price) {
      setHargaStarterPack(price[1].minimumPrice);
      setHargaGantiKartu(price[0].minimumPrice);
      setHargaPascabayar(price[2].minimumPrice);
      setHargaDeviceBundling(price[3].minimumPrice);
    }
  };

  const renderContent = () => (
    <Animatable.View animation="slideInUp" style={styles.panel}>
      <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
        <View style={styles.search}>
          <TouchableOpacity
            style={{ flexDirection: 'row' }}
            onPress={() => RootNavigation.navigate('Search')}>
            <Ionicons name="search" size={18} color={'grey'} />
            <Text
              style={{
                fontSize: 18,
                color: '#dbdbdb',
                fontWeight: 'bold',
                marginLeft: 20
              }}>
              Cari Product..{' '}
            </Text>
          </TouchableOpacity>
        </View>

      </View>
      <View>
        <SliderHome />
      </View>
      <View style={styles.sheetContainer}>
        <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
          <TouchableOpacity
            style={styles.menuContainer}
            onPress={() => navigation.navigate('GantiSIM')}>
            <Image
              style={styles.menuImage}
              source={require('../../../assets/SimCard.jpg')}
            />
            <Text style={styles.fontMenu}>Ganti Kartu</Text>
            <NumberFormat
              value={hargaGantiKartu}
              displayType={'text'}
              prefix={'Rp. '}
              renderText={(value) => (
                <Text style={styles.fontHarga}>{value}</Text>
              )}
              thousandSeparator={'.'}
              decimalSeparator={','}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.menuContainer}
            onPress={() => dispatch(navigatePascabayar(0))}>
            <Image
              style={styles.menuImage}
              source={require('../../../assets/pascabayar.jpg')}
            />
            <Text style={styles.fontMenu}>Beli Pascabayar</Text>
            <Text style={styles.fontHarga}>Mulai dari</Text>
            <NumberFormat
              value={hargaPascabayar}
              displayType={'text'}
              prefix={'Rp. '}
              renderText={(value) => (
                <Text style={styles.fontHarga}>{value}</Text>
              )}
              thousandSeparator={'.'}
              decimalSeparator={','}
            />
          </TouchableOpacity>
        </View>
        <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
          <TouchableOpacity
            style={styles.menuContainer}
            onPress={() => dispatch(navigateStarterpack(0))}>
            <Image
              style={styles.menuImage}
              source={require('../../../assets/starterPack.jpg')}
            />
            <Text style={styles.fontMenu}>Starter Pack</Text>
            <Text style={styles.fontHarga}>Mulai dari</Text>
            <NumberFormat
              value={hargaStarterPack}
              displayType={'text'}
              prefix={'Rp. '}
              renderText={(value) => (
                <Text style={styles.fontHarga}>{value}</Text>
              )}
              thousandSeparator={'.'}
              decimalSeparator={','}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.menuContainer}
            onPress={() => dispatch(navigateBundling(0))}>
            <Image
              style={styles.menuImage}
              source={require('../../../assets/DeviceBundling.jpg')}
            />
            <Text style={styles.fontMenu}>Device Bundling</Text>
            <Text style={styles.fontHarga}>Mulai dari</Text>
            <NumberFormat
              value={hargaDeviceBundling}
              displayType={'text'}
              prefix={'Rp. '}
              renderText={(value) => (
                <Text style={styles.fontHarga}>{value}</Text>
              )}
              thousandSeparator={'.'}
              decimalSeparator={','}
            />
          </TouchableOpacity>
        </View>
      </View>
    </Animatable.View>
  );

  const renderHeader = () => (
    <Animatable.View animation="slideInUp" style={styles.header}>
      <View style={styles.panelHeader}>
        <View style={styles.panelHandle} />
      </View>
    </Animatable.View>
  );
  return (
    <LinearGradient
      start={{ x: 0, y: 0 }}
      end={{ x: 1, y: 0 }}
      colors={['#e52d27', '#b31217']}
      style={styles.container}>
      <View style={styles.homeHeader}>
        <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
          <TouchableOpacity onPress={() => navigation.navigate('Profil')}>
            <ProfileIcon />
          </TouchableOpacity>
          <Text style={styles.fontHalo}>
            Halo, <Text style={{ fontWeight: 'bold' }}>{profile.name}</Text>
          </Text>
        </View>
        <View>
          <TouchableOpacity onPress={() => navigation.navigate('Cart')}>
            <CartIcon />
          </TouchableOpacity>
        </View>
      </View>
      <Image
        style={styles.homeImage}
        source={require('../../../assets/home.png')}
      />
      <BottomSheet
        ref={sheetRef}
        snapPoints={
          Platform.OS === 'ios' ?
            [height * 0.85, height * 0.6, height * 0.35]
            : [height * 0.8, height * 0.6, height * 0.35]
        }
        renderContent={renderContent}
        renderHeader={renderHeader}
        initialSnap={0}
      />
    </LinearGradient>
  );
};
export default Homescreen;
