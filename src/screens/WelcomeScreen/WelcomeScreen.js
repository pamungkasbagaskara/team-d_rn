import React from 'react';
import { View, Text } from 'react-native';
import { Button } from 'react-native-paper';
import * as Animatable from 'react-native-animatable';
import LinearGradient from 'react-native-linear-gradient';
import TextTicker from 'react-native-text-ticker';
import Feather from 'react-native-vector-icons/Feather';

import MyCarousel from '../../components/Carousel';

import styles from './WelcomeStyles';

const WelcomeScreen = ({ navigation }) => {
  const registerClick = () => {
    navigation.navigate('Registers');
  };
  const loginClick = () => {
    navigation.navigate('Login');
  };

  return (
    <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['#e52d27', '#b31217']} style={styles.container}>
      <View style={{ flex: 1.7 }}>
        <MyCarousel />
      </View>
      <View style={styles.footer}>
        <View style={styles.ticker}>

          <TextTicker style={{ fontSize: 16 }} duration={10000}
            loop
            bounce
            repeatSpacer={50}
            marqueeDelay={1000}>
            <Feather name="bell" color="#DE3030" size={16} style={styles.icon}/>
            Dapatkan diskon device bundling telkomsel, hanya hari ini! unlimited 1 tahun hanya 100 rb
          </TextTicker>
        </View>
        <View style={styles.text_logo}>
          <Animatable.Image
            animation="slideInUp"
            direction="normal"
            source={require('../../../assets/image12.png')}
            style={styles.logo}
            resizeMode="contain"/>
          <Animatable.Text animation="slideInUp" iterationCount={1} direction="normal" style={styles.text}>Solutions</Animatable.Text>
        </View>
        <Animatable.View animation="fadeInUpBig">

          <Button onPress={registerClick}
            style={styles.buttonSignup}
            mode="contained"
            color ="#FFFFFF" >
            <Text style={styles.text_footer}> Registrasi sekarang! </Text>
          </Button>

          <Button onPress={loginClick}
            style={styles.buttonSignIn}
            mode="text"
            color ="#FFFFFF"> Sudah punya akun? Masuk!
          </Button>
        </Animatable.View>
      </View>
    </LinearGradient>
  );
};

export default WelcomeScreen;

