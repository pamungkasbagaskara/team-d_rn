import React, { } from 'react';
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Platform,
  Keyboard,
  TextInput,
  ScrollView
} from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import { HelperText } from 'react-native-paper';
import Spinner from 'react-native-loading-spinner-overlay';
import * as Animatable from 'react-native-animatable';
import LinearGradient from 'react-native-linear-gradient';
import { useSelector, useDispatch } from 'react-redux';

import { goLogin } from '../../store/actions/loginAction';

import styles from './LoginStyles';

const LoginScreen = ({ navigation }) => {
  const isLoading = useSelector((state) => state.loginStore.isLoading);
  const dispatch = useDispatch();
  const [data, setData] = React.useState({
    hidePsw: null,
    token: null,
    refreshToken: null,
    secureTextEntry: true,
    email: null,
    password: null,
    onError: false,
    errorMsg: null
  });

  const registerClick = () => {
    navigation.navigate('Registers');
  };

  const updateSecureTextEntry = () => {
    setData({
      ...data,
      hidePsw: !data.hidePsw
    });
  };

  const passwordChange = (val) => {
    if (val.length > 4) {
      setData({
        ...data,
        password: val,
        check_passwordChange: true
      });
    } else {
      setData({
        ...data,
        phonenumber: val,
        check_passwordChange: false
      });
    }
  };

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      style={{ flex: 1 }}>
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['#e52d27', '#b31217']} style={styles.container}>
          <ScrollView >

            <View style={styles.header}>
              <View style={styles.divider}>
                <Image
                  style={styles.pictureView}
                  source={require('../../../assets/lologin.png')} />
              </View>
            </View>
            <View style={styles.body}>
              <Animatable.View animation="fadeInUpBig">
                <View style={styles.action}>
                  <TextInput
                    placeholder="Your Email"
                    autoCapitalize="none"
                    placeholderTextColor="#DE3030"
                    style={styles.textInput}
                    onChangeText={(val) => setData({ ...data, email: val })}
                  />
                  <Feather name="mail" color="#DE3030" size={20} style={styles.icon} />
                </View>
                <View style={styles.action}>
                  <TextInput
                    placeholder="Your Password"
                    placeholderTextColor="#DE3030"
                    style={styles.textInput}
                    secureTextEntry={!data.hidePsw}
                    onChangeText={(val) => passwordChange(val)}
                  />
                  <Feather name="key" color="#DE3030" size={20} style={styles.icon} />
                  <TouchableOpacity disabled={data.isLoading} onPress={updateSecureTextEntry}
                    style={styles.eye}>
                    {data.hidePsw ?
                      <Feather name="eye" color="black" size={20} />
                      : <Feather name="eye-off" color="black" size={20} />
                    }
                  </TouchableOpacity>
                </View>
              </Animatable.View>
              <HelperText style={styles.textError} type="error" visible={data.onError}>
                {data.errorMsg ?
                  data.errorMsg
                  : 'Something Went Wrong!'}
              </HelperText>
            </View>
            <View style={styles.footer}>
              <View style={{ flexDirection: 'row', alignSelf: 'flex-end' }}>
                <TouchableOpacity onPress={() => dispatch(goLogin(data.email, data.password))}>
                  <Text style={styles.textPrivate}>Masuk </Text>
                  <Feather name="arrow-right-circle" color="white" size={40} style={styles.icons} />
                </TouchableOpacity>
              </View>
              <View style={{ flexDirection: 'row', alignSelf: 'flex-end' }}>
                <TouchableOpacity onPress={() => registerClick()}>
                  <Text style={styles.text}>Masih belum punya akun? Daftar yuk </Text>
                </TouchableOpacity>
              </View>
              <View style={{ flexDirection: 'row', alignSelf: 'flex-end' }}>
                <TouchableOpacity onPress={() => navigation.navigate('Forgot')}>
                  <Text style={styles.text}>Lupa password? </Text>
                </TouchableOpacity>
              </View>
            </View>
            <Spinner
              visible={isLoading}
              textContent={'Loading...'}
              textStyle={styles.spinnerTextStyle}
              animation="fade"
            />
          </ScrollView>
        </LinearGradient>
      </TouchableWithoutFeedback>
    </KeyboardAvoidingView>
  );
};

export default LoginScreen;
