import React, { useState } from 'react';
import { View, Text, Image,Dimensions,Modal,TextInput,TouchableOpacity } from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import Spinner from 'react-native-loading-spinner-overlay';
const { width, height } = Dimensions.get('window');
import * as Animatable from 'react-native-animatable';
import LinearGradient from 'react-native-linear-gradient';
import { useSelector,useDispatch } from 'react-redux';
import { ScrollView } from 'react-native-gesture-handler';

import { goOTP, goValidate } from '../../store/actions/forgotAction';

import styles from './RegisterStyles';

const ForgotScreen = ({ navigation }) => {
  const isLoading = useSelector((state) => state.profileStore.isLoading);
  const [] = useState(false);
  const [show, setShow] = useState(false);
  const [OTP, setOTP] = useState(null);
  const dispatch = useDispatch();
  const [data, setData] = React.useState({
    password: null,
    OTP: null,
    email: null,
    check_emailInputChange: false,
    isLoading: false,
    secureTextEntry: true,
    onError: false,
    onMessage: null,
    errorMsg: null,
    token: null,
    refresh_token: null
  });

  const SendOTP = ()=>{
    setData({ ...data,isLoading: true });
    setShow(true);
    let input = {
      email: data.email
    };
    dispatch(goOTP(input));
  };

  const emailInputChange = (val) => {
    let reg = /^\w+([.-]?\w+)*@\w+([\\.-]?\w+)*(\.\w{2,3})+$/;
    let isValid = reg.test(val);
    if (val.trim().length !== '' && isValid === true) {
      setData({
        ...data,
        email: val,
        check_emailInputChange: true
      });
    } else {
      setData({
        ...data,
        email: val,
        check_emailInputChange: false
      });
    }
  };

  const validation = ()=>{
    setShow(false);
    dispatch(goValidate(data.email,OTP));
  };


  return (
    <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['#e52d27', '#b31217']} style={styles.container}>

      <Modal
        animationType="slide"
        transparent={true}
        visible={show}
        onRequestClose = {()=>setShow(false)}
      >

        <View style={{ backgroundColor: '#000000aa',flex: 1 }}>
          <ScrollView>
            <View style={{ backgroundColor: '#ffffff', margin: width * 0.03, padding: width * 0.05,borderRadius: width * 0.05,marginTop: height * 0.4 }}>
              <TouchableOpacity hitSlop={{ top: width * 0.08, bottom: width * 0.08, left: width * 0.08, right: width * 0.08 }} onPress={()=>setShow(false)}>
                <Feather name="x-circle" color="#DE3030" size={20} style={styles.modalicon}/>
              </TouchableOpacity>
              <Text style={styles.textPrivates}>Kami mengirimkan kode OTP</Text>
              <Text style={styles.textPrivates}>Cek email yuk!</Text>
              <TextInput
                placeholderTextColor="gray"
                keyboardType="number-pad"
                placeholder="_ _ _ _ _ _"
                style={styles.modal}
                onChangeText={item => setOTP(item)}
              />
              <TouchableOpacity style={styles.modalbutton} onPress={validation}>
                <Text style={{ textAlign: 'center',fontSize: 15,padding: 8,fontWeight: 'bold',color: '#DE3030' }}>Submit</Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </View>

      </Modal>
      <ScrollView>
        <View style={styles.body}>
          <View style={styles.divider}>
            <Text style={styles.text_header}>Lupa Password?</Text>
            <Image
              style={styles.pictureView}
              source={require('../../asset/image/Forgot.png')} />

          </View>
        </View>

        <View style={styles.footer}>
          <View style={styles.actions}>
            <TextInput
              placeholder="Masukkan Email Terdaftar"
              placeholderTextColor = "#DE3030"
              autoCapitalize="none"
              keyboardType={'email-address'}
              textContentType={'emailAddress'}
              style={styles.forgot}
              onChangeText={(val) => emailInputChange(val)}
            />
            <Feather name="mail" color="#DE3030" size={20} style={styles.icon}/>
            {data.check_UsernameChange ?
              <Animatable.View
                animation="bounceIn"
              >
                <Feather
                  name="check-circle"
                  color="green"
                  size={20}
                  style={styles.check}
                />
              </Animatable.View>
              : null}
          </View>
          <View style={{ flexDirection: 'row',alignSelf: 'flex-end' }}>
            <TouchableOpacity onPress={()=>SendOTP()}>
              <Text style={styles.textPrivate}>Submit </Text>
              <Feather name="arrow-right-circle" color="white" size={40} style={styles.icons}/>
            </TouchableOpacity>
          </View>
          <View style={{ flexDirection: 'row',alignSelf: 'flex-end',paddingTop: 10 }}>
            <TouchableOpacity onPress={()=> navigation.goBack()}>
              <Text style={styles.textPrivate}>Kembali </Text>
              <Feather name="arrow-left-circle" color="white" size={40} style={styles.icons}/>
            </TouchableOpacity>
          </View>
          <Spinner
            visible={isLoading}
            textContent={'Loading...'}
            textStyle={styles.spinnerTextStyle}
            animation="fade"
          />
        </View>
      </ScrollView>
    </LinearGradient>
  );
};

export default ForgotScreen;
