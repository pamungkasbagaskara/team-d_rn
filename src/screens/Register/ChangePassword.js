import React, { useState } from 'react';
import { View, Text, Image, Dimensions, TextInput, TouchableOpacity } from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
const { height } = Dimensions.get('window');
import LinearGradient from 'react-native-linear-gradient';
import { useDispatch } from 'react-redux';

import { goChangePassword } from '../../store/actions/forgotAction';

import styles from './RegisterStyles';

const ChangePassword = ({ }) => {
  const [] = useState(false);
  const [] = useState(false);
  const [] = useState();
  const dispatch = useDispatch();
  const [data, setData] = React.useState({
    password: null,
    isLoading: false,
    secureTextEntry: true,
    onError: false,
    onMessage: null,
    errorMsg: null,
    token: null,
    refresh_token: null,
    check_passwordChange: Boolean
  });

  const SendOTP = () => {
    let input = {
      password: data.password
    };
    dispatch(goChangePassword(input));
  };

  const passwordChange = (val) => {
    if (val.length > 4) {
      setData({
        ...data,
        password: val,
        check_passwordChange: true
      });
    } else {
      setData({
        ...data,
        phonenumber: val,
        check_passwordChange: false
      });
    }
  };
  const updateSecureTextEntry = () => {
    setData({
      ...data,
      hidePsw: !data.hidePsw
    });
  };
  return (
    <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['#e52d27', '#b31217']} style={styles.container}>
      <View style={styles.body}>
        <View style={styles.divider}>
          <Text style={styles.text_header}>Satu Langkah Lagi</Text>
          <Image
            style={styles.picturesView}
            source={require('../../asset/image/changepass.png')} />

        </View>
      </View>

      <View style={styles.footer}>
        <View style={styles.action}>
          <TextInput
            placeholder="Masukkan Password"
            placeholderTextColor="#DE3030"
            secureTextEntry={!data.hidePsw}
            autoCapitalize="none"
            style={styles.textInput}
            onChangeText={(val) => passwordChange(val)}
          />
          <Feather name="key" color="#DE3030" size={20} style={styles.icon} />
          <TouchableOpacity disabled={data.isLoading} onPress={updateSecureTextEntry} style={styles.eye}>
            {data.hidePsw ?
              <Feather name="eye" color="black" size={20} />
              : <Feather name="eye-off" color="black" size={20} />
            }
          </TouchableOpacity>
          {data.check_passwordChange ?
            null
            : <Text style={styles.textError}>Password minimum contain 5 characters!</Text>}

        </View>
        <View style={{ flexDirection: 'row', alignSelf: 'flex-end', marginTop: height * 0.05 }}>
          <TouchableOpacity onPress={SendOTP}>
            <Text style={styles.textPrivate}>Ganti Password </Text>
            <Feather name="arrow-right-circle" color="white" size={40} style={styles.icons} />
          </TouchableOpacity>
        </View>

      </View>
    </LinearGradient>
  );
};

export default ChangePassword;
