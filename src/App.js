import React, { useState, useEffect, useRef } from 'react';
import { AppState, Alert, BackHandler } from 'react-native';
import { Provider } from 'react-redux';
import Config from 'react-native-config';
import codePush from 'react-native-code-push';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { PersistGate } from 'redux-persist/integration/react';

import Navigation from './navigation/index';
import store from './store';
import { persistor } from './store';

import { refreshTokenHandler, getAppVersion } from './utils/fetchAPI';

const App = () => {

  const appState = useRef(AppState.currentState);
  // eslint-disable-next-line no-unused-vars
  const [appStateVisible, setAppStateVisible] = useState(appState.current);
  const [updateVersion, setUpdateVersion] = useState(false);
  const [isTokenExist , setIsTokenExist] = useState(false);

  useEffect(()=>{
    getToken();
    checkVersion();
  }, [isTokenExist, updateVersion]);


  const getToken = async () => {
    const token = await AsyncStorage.getItem('token');
    if (token){
      setIsTokenExist(true);
    }
    else {
      setIsTokenExist(false);
    }
  };

  useEffect(()=> {
    AppState.addEventListener('change', _handleAppStateChange);
    codePush.sync({ installmode: codePush.InstallMode.IMMEDIATE });

    return () => {
      AppState.removeEventListener('change', _handleAppStateChange);
    };
  });

  const _handleAppStateChange = (nextAppState) => {
    if (
      appState.current.match(/inactive|background/) &&
      nextAppState === 'active'
    ) {
      if (isTokenExist){
        refreshTokenHandler();
      }
    }

    appState.current = nextAppState;
    setAppStateVisible(appState.current);
  };

  const checkVersion = async () => {
    const version = await getAppVersion();
    if (parseFloat(Config.APP_VERSION) >= parseFloat(version)) {
      setUpdateVersion(false);
    }
    else {
      setUpdateVersion(true);
    }
  };

  return (
    <Provider store={store}>
      {updateVersion &&
        (
          Alert.alert('Aplikasi belum update', `Apakah anda ingin update aplikasi secara otomatis ke versi ${Config.APP_VERSION} ?`, [
            {
              text: 'TIDAK',
              onPress: () => BackHandler.exitApp(),
              style: 'cancel'
            },
            { text: 'YA', onPress: () => codePush.restartApp() }
          ])
        )
      }
      <PersistGate loading={null} persistor={persistor}>
        <Navigation />
      </PersistGate>
    </Provider>

  );
};


let codePushOptions = { checkFrequency: codePush.CheckFrequency.ON_APP_RESUME };
// App = codePush(codePushOptions)(App);
export default codePush(codePushOptions)(App);
