import React, { useState } from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity
} from 'react-native';
import numbro from 'numbro';
import { useSelector, useDispatch } from 'react-redux';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

import { inputDetilProduct } from '../store/actions/productAction';

const { width } = Dimensions.get('window');

export default function ProductCardList(data) {
  const dispatch = useDispatch();
  const isLoading = useSelector((state) => state.productStore.isLoading);

  const [subProductLocal] = useState(data.subProduct);
  const [productLocal] = useState(data.product.data.result);

  if (productLocal) {
    var filtered = productLocal.filter((a) => a.subProduct === subProductLocal);
  }
  return (
    <>
      {filtered &&
        filtered.map((item, i) => {
          return (
            <TouchableOpacity
              onPress={() => dispatch(inputDetilProduct(item))}
              key={i}>
              {isLoading === false ? (
                <View style={styles.card}>
                  <View style={{ alignItems: 'center',justifyContent: 'center', flex: 1 }}>
                    <Image style={styles.poster} source={{ uri: item.poster }} />
                  </View>
                  <View style={{ flexDirection: 'column' }}>
                    <Text style={styles.subProduct}>{item.subProduct}</Text>
                    <Text style={styles.name} note numberOfLines={2}>
                      {item.name}
                    </Text>
                    <View style={styles.priceWrap}>
                      <Text style={styles.price}>
                        Rp.{' '}
                        {numbro(item.price).format({ thousandSeparated: true })}
                      </Text>
                    </View>
                  </View>
                </View>
              ) : (
                <SkeletonPlaceholder speed={1000} backgroundColor={'#e0e0e0'}>
                  <View
                    style={{
                      height: 0.45 * width,
                      width: 0.45 * width,
                      paddingTop: 30,
                      marginTop: 10,
                      alignItems: 'center',
                      borderTopLeftRadius: 20,
                      borderTopRightRadius: 20,
                      marginHorizontal: 0.02 * width
                    }}
                  />

                  <View
                    style={{
                      width: 0.3 * width,
                      height: 20,
                      borderRadius: 100,
                      marginVertical: 5,
                      marginHorizontal: 0.02 * width
                    }}
                  />
                  <View
                    style={{
                      width: 0.4 * width,
                      height: 20,
                      borderRadius: 100,
                      marginVertical: 5,
                      marginHorizontal: 0.02 * width
                    }}
                  />
                  <View
                    style={{
                      width: 0.3 * width,
                      height: 20,
                      borderRadius: 100,
                      marginTop: 5,
                      marginBottom: 30,
                      marginHorizontal: 0.02 * width
                    }}
                  />
                </SkeletonPlaceholder>
              )}
            </TouchableOpacity>
          );
        })}
    </>
  );
}

const styles = StyleSheet.create({
  poster: {
    height: 0.2 * width,
    width: 0.2 * width,
    paddingTop: 30,
    alignItems: 'center'
  },
  checkBlue: {
    height: 12,
    width: 12
  },
  card: {
    marginHorizontal: 0.02 * width,
    marginTop: 10,
    width: 0.95 * width,
    backgroundColor: 'white',
    borderRadius: 20,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    flexDirection: 'row',
    elevation: 3
  },
  name: {
    fontSize: 16,
    fontWeight: 'bold',
    marginLeft: 10,
    marginBottom: 5,
    width: 0.7 * width
  },
  subProduct: {
    fontWeight: 'bold',
    marginTop: 5,
    marginLeft: 10,
    marginBottom: 10,
    fontSize: 14,
    color: '#F18F40'
  },
  price: {
    fontSize: 16,
    color: 'white',

    fontWeight: 'bold'
  },
  priceWrap: {
    margin: 10,
    backgroundColor: '#DE3030',
    borderRadius: 20,
    justifyContent: 'center',
    alignSelf: 'flex-start',
    padding: 5
  }
});
