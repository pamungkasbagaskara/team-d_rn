import React from 'react';
import {
  StyleSheet,
  View,
  Dimensions
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
const { width, height } = Dimensions.get('window');

export const CartIcon = () => {
  return (

    <View style={styles.cartContainer}>
      <Ionicons name="cart-outline" size={25} color={'#FFFAF4'} />
    </View>
  );
};

export const ProfileIcon = () => {
  return (

    <View style={styles.profileContainer}>
      <Ionicons name="person-outline" size={25} color={'#FFFAF4'} />
    </View>
  );
};

export const BackIcon = () => {
  return (
    <View style={styles.BackContainer}>
      <Ionicons name="chevron-back" size={25} color={'#FFFAF4'} />
    </View>
  );
};

export const NextIcon = () => {
  return (
    <View style={styles.NextContainer}>
      <Ionicons name="chevron-forward-outline" size={25} color={'#FFFAF4'} />
    </View>
  );
};


const styles = StyleSheet.create({
  cartContainer: {
    backgroundColor: 'rgba(255,255,255,0.3)',
    width: 0.1221 * width,
    height: 0.049107 * height,
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center'
  },
  profileContainer: {
    backgroundColor: 'rgba(255,255,255,0.3)',

    width: 40,
    height: 40,
    borderRadius: 40 / 2,
    alignItems: 'center',
    justifyContent: 'center'
  },
  BackContainer: {
    backgroundColor: 'rgba(255,255,255,0.3)',
    width: 40,
    height: 40,
    borderRadius: 40 / 2,
    alignItems: 'center',
    justifyContent: 'center'
  },
  NextContainer: {
    backgroundColor: 'red',
    width: 50,
    height: 50,
    borderRadius: 50 / 2,
    alignItems: 'center',
    justifyContent: 'center'
  }

});
