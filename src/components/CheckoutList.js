import React from 'react';
import { View, Text, StyleSheet, Image, Dimensions } from 'react-native';
import NumberFormat from 'react-number-format';
const { width } = Dimensions.get('window');

const CheckoutList = ({ data }) => {
  const image = { uri: `${data.poster}` };
  return (
    <View style={styles.cardContainer}>
      <View style={styles.imageContainer}>
        {data.poster ? <Image source={image} style={styles.logo} /> : <Image source={require('../asset/image/simcard.png')} style={styles.logo} />}
      </View>
      <View style={styles.textContainer}>

        <View style={styles.actionContainer}>
          {data.productCategoryId !== 1 && (
            <Text style={{ fontWeight: 'bold', marginTop: -15, paddingHorizontal: 5 }}>x{data.qty}</Text>
          )
          }
        </View>
        {data && <Text style={styles.labelCategory}>{data.name}</Text>}
        {data && <Text style={styles.labelName} >{data.number}</Text>}
        <View style={styles.priceContainer}>
          {data.price && data.price > 0 ?
            (
              <NumberFormat value={data.price}
                displayType={'text'} prefix={'Rp. '}
                renderText={value => <Text style={styles.labelPrice}>{value}</Text>}
                thousandSeparator={'.'} decimalSeparator={','}
              />
            )
            : (
              <Text style={styles.labelPrice}>Free</Text>
            )
          }
        </View>
      </View>
    </View>
  );
};

export default CheckoutList;

const styles = StyleSheet.create({
  cardContainer: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#FFF',
    borderRadius: 20,
    paddingVertical: 20,
    paddingHorizontal: 5,
    alignItems: 'center',
    marginVertical: 5
  },
  imageContainer: {
    flex: 1,
    paddingHorizontal: 5
  },
  logo: {
    width: 100,
    height: 100
  },
  textContainer: {
    flex: 2,
    paddingLeft: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  labelCategory: {
    color: '#F18F40'
  },
  labelName: {
    fontSize: 18,
    fontWeight: 'bold',
    maxWidth: 150,
    textAlign: 'center'
  },
  priceContainer: {
    backgroundColor: '#de3030',
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 100,
    marginVertical: 5,
    maxWidth: 200
  },
  labelPrice: {
    fontSize: width * 0.035,
    fontWeight: 'bold',
    color: '#FFF',
    textAlign: 'center'
  },
  actionContainer: {
    flexDirection: 'row',
    paddingLeft: 10,
    justifyContent: 'flex-start',
    alignItems: 'flex-end',
    alignSelf: 'flex-end',
    paddingBottom: 20
  }
});
