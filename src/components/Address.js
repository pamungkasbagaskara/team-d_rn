import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, FlatList, Dimensions } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { useDispatch } from 'react-redux';
import Feather from 'react-native-vector-icons/Feather';

import { selectAddress } from '../store/actions/addressAction';
import * as RootNavigation from '../navigation/RootNavigation';

import AddressList from './AddressList';

const { width, height } = Dimensions.get('window');

const Address = ({ data, onPress }) => {
  const dispatch = useDispatch();

  return (
    <View style={styles.panel}>
      <View style={styles.panelTitle}>
        <TouchableOpacity onPress={onPress}>
          <Ionicons
            name="chevron-back-circle"
            size={30}
            color={'#CD413A'}
            style={{ paddingRight: 10, opacity: 0.3 }}
          />
        </TouchableOpacity>
        <Text style={styles.title}>Alamat Pengiriman</Text>
      </View>
      <View style={styles.panelAddress}>
        <View style={styles.labelContainer}>
          <FlatList
            data={data}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item }) => (
              <AddressList data={item} onPress={() => dispatch(selectAddress(item))} />
            )}
            showsVerticalScrollIndicator={false}
          />
        </View>
      </View>

      <TouchableOpacity
        style={styles.buttonContainer}
        onPress={() => {
          RootNavigation.navigate('Address');
        }}>
        <Text style={styles.buttonText}>Tambah Alamat Baru</Text>
        <Feather
          name="arrow-right-circle"
          color="white"
          size={40}
          style={styles.icons}
        />
      </TouchableOpacity>
    </View>
  );
};

export default Address;

const styles = StyleSheet.create({
  cardContainer: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#FFF',
    borderRadius: 20,
    paddingVertical: 20,
    paddingHorizontal: 5,
    alignItems: 'center',
    marginVertical: 5
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#de3030',
    paddingHorizontal: 30,
    paddingVertical: 10,
    borderRadius: 100,
    marginVertical: 5,
    maxWidth: 250
  },
  buttonText: {
    color: '#FFF',
    fontWeight: 'bold',
    paddingHorizontal: 10
  },
  container: {
    flex: 1,
    backgroundColor: '#CD413A',
    alignItems: 'center'
  },
  panel: {
    marginTop: -10,
    height: height * 0.925,
    paddingVertical: 20,
    paddingBottom: height * 0.1,
    paddingHorizontal: 20,
    backgroundColor: '#FEFAF5'
  },
  header: {
    backgroundColor: '#FEFAF5',
    shadowColor: '#000000',
    paddingTop: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20
  },
  panelHeader: {
    alignItems: 'center'
  },
  panelHandle: {
    width: 100,
    height: 8,
    borderRadius: 4,
    backgroundColor: '#F18F40',
    marginBottom: 5
  },
  panelTitle: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  title: {
    fontSize: width * 0.075,
    fontWeight: 'bold',
    color: '#CD413A',
    width: '75%',
    textAlign: 'center'
  },
  panelAddress: {
    flex: 2,
    paddingVertical: 10
  },
  labelContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  label: {
    fontSize: width * 0.05,
    fontWeight: 'bold',
    color: '#F18F40',
    marginVertical: 5
  },
  addressTitle: {
    fontWeight: 'bold',
    fontSize: 16,
    color: '#262626'
  },
  address: {
    fontWeight: '500',
    fontSize: 13,
    color: '#262626'
  },
  deliveryFees: {
    flex: 1,
    paddingVertical: 20
  },
  panelPayment: {
    flex: 9,
    paddingVertical: 20
  }
});
