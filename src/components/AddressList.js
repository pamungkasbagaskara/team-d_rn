import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Dimensions } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAweSome from 'react-native-vector-icons/FontAwesome';

import { deleteAddress, selectEditAddress } from '../store/actions/addressAction';

const { width } = Dimensions.get('window');

const AddressList = ({ data, onPress }) => {
  const dispatch = useDispatch();
  const selectedAddress = useSelector((state) => state.addressStore.selectedAddress);

  const editHandler = () => {
    dispatch(selectEditAddress(data));
  };

  const deleteHandler = () => {
    dispatch(deleteAddress({ id: data.id }));
  };

  return (
    <View style={selectedAddress && selectedAddress.id === data.id ? styles.cardContainerActive : styles.cardContainer}>
      <View style={styles.buttonContainer}>
        <TouchableOpacity style={styles.deleteButton} onPress={deleteHandler}>
          <Ionicons name="trash-sharp" size={20} color={'#FFF'} />
        </TouchableOpacity>
      </View>

      <View style={{ flexDirection: 'row' }}>
        <TouchableOpacity style={styles.editButton} onPress={editHandler}>
          <FontAweSome name="edit" size={30} color={'#F18F40'} style={{ paddingHorizontal: 10, opacity: 0.5 }} />
        </TouchableOpacity>

        <TouchableOpacity onPress={onPress}>
          <Text style={styles.addressTitle}>
            {data.addressName}
          </Text>
          <Text style={styles.address}>
            {data.street}, {data.district}, {data.regency}, {data.state}, {data.province}, {data.postalCode}
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default AddressList;

const styles = StyleSheet.create({
  cardContainer: {
    backgroundColor: '#FFF',
    borderRadius: 20,
    paddingVertical: 20,
    paddingHorizontal: 5,
    alignItems: 'flex-start',
    marginVertical: 15
  },
  cardContainerActive: {
    backgroundColor: '#FFF',
    borderRadius: 20,
    paddingVertical: 20,
    paddingHorizontal: 5,
    alignItems: 'flex-start',
    marginVertical: 15,
    borderWidth: 1,
    borderColor: '#F18F40'
  },
  labelContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  label: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#F18F40',
    marginVertical: 5
  },
  addressTitle: {
    fontWeight: 'bold',
    fontSize: 16,
    color: '#262626',
    textAlign: 'left',
    alignItems: 'flex-start'
  },
  address: {
    fontWeight: '500',
    fontSize: 13,
    width: width * 0.7,
    color: '#262626'
  },
  buttonContainer: {
    marginTop: -30,
    marginRight: 5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    alignSelf: 'flex-end'
  },
  deleteButton: {
    padding: 5,
    backgroundColor: '#CD413A',
    borderRadius: 50
  },
  editButton: {
    padding: 5,
    borderRadius: 50
  }

});
