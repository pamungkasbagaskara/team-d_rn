import React, { useRef, useState, useEffect } from 'react';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import {
  View,
  Dimensions,
  StyleSheet,
  TouchableOpacity,
  SafeAreaView
} from 'react-native';

import { carouselData } from '../data/Data';

import CarouselItem from './CarouselItem';
const { width, height } = Dimensions.get('window');



const MyCarousel = () => {
  const [activeSlide, setActiveSlide] = useState(0);
  const [entries, setEntries] = useState(carouselData);
  const carouselRef = useRef(null);

  const goForward = () => {
    carouselRef.current.snapToNext();
  };

  useEffect(() => {
    setEntries(carouselData);
  }, []);

  const renderPagination = () => (
    <Pagination
      dotsLength={entries.length}
      activeDotIndex={activeSlide}
      dotStyle={styles.dotStyle}
      containerStyle={styles.paginationContainer}
    />
  );

  return (
    <SafeAreaView style={styles.container}>
      <View style={{ flex: 1,height: height * 0.9 }}>
        <TouchableOpacity onPress={goForward} />
        <Carousel
          ref={carouselRef}
          sliderWidth={width}
          sliderHeight={width + 100}
          itemWidth={width - 20}
          data={entries}
          loop={true}
          onSnapToItem={(index) => setActiveSlide(index)}
          autoplay={true}
          renderItem={({ item }) => {
            return <CarouselItem item={item} />;
          }}
        />
      </View>

      {renderPagination()}
    </SafeAreaView>
  );
};

export default MyCarousel;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: height
  },
  dotView: {
    flexDirection: 'row',
    justifyContent: 'center'
  },
  paginationContainer: {

  },
  dotStyle: {
    backgroundColor: 'white'
  }

});
