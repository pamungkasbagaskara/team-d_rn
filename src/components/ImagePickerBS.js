import React from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Dimensions
} from 'react-native';

const { height } = Dimensions.get('window');

export const ImagePickerBSContent = ({ takePhotoAction, chooseImageAction, cancelAction }) => (
  <View style={styles.panelContainer}>
    <Text style={styles.title}>Upload Photo</Text>

    <TouchableOpacity style={styles.button} onPress={takePhotoAction}>
      <Text style={styles.buttonText}>Take Photo</Text>
    </TouchableOpacity>

    <TouchableOpacity style={styles.button} onPress={chooseImageAction}>
      <Text style={styles.buttonText}>Choose From Library</Text>
    </TouchableOpacity>

    <TouchableOpacity
      style={styles.button}
      onPress={cancelAction}>
      <Text style={styles.buttonText}>Cancel</Text>
    </TouchableOpacity>
  </View>
);

export const ImagePickerBSHeader = () => (
  <View style={styles.imageheader}>
    <View style={styles.panelHeader}>
      <View style={styles.panelHandle} />
    </View>
  </View>
);

const styles = StyleSheet.create({
  panelContainer: {
    backgroundColor: '#FFF',
    alignItems: 'center',
    paddingBottom: 20
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#DE3030',
    paddingBottom: 30
  },
  label: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#DE3030'
  },
  panel: {
    height: height * 0.8,
    paddingVertical: 20,
    paddingHorizontal: 30,
    backgroundColor: '#FEFAF5'
  },
  header: {
    backgroundColor: '#FEFAF5',
    shadowColor: '#000000',
    paddingTop: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20
  },
  imageheader: {
    backgroundColor: '#FFF',
    shadowColor: '#000000',
    paddingTop: 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20
  },
  panelHeader: {
    alignItems: 'center'
  },
  panelHandle: {
    width: 40,
    height: 8,
    borderRadius: 4,
    backgroundColor: '#00000040',
    marginBottom: 10
  },
  panelTitle: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  panelPhoto: {
    width: '100%',
    backgroundColor: '#E6E6E6',
    height: 250,
    marginTop: 75,
    alignItems: 'center',
    justifyContent: 'center'
  },
  panelButton: {
    paddingVertical: 10,
    width: '100%',
    alignItems: 'center'
  },
  button: {
    borderColor: '#DE3030',
    paddingVertical: 10,
    borderWidth: 1,
    paddingHorizontal: 10,
    borderRadius: 20,
    justifyContent: 'center',
    flexDirection: 'row',
    backgroundColor: '#FFF',
    elevation: 2,
    marginVertical: 5
  },
  buttonText: {
    fontSize: 17,
    fontWeight: 'bold',
    color: '#DE3030',
    paddingHorizontal: 20
  }
});
