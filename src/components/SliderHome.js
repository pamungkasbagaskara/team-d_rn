import React from 'react';
import { Text, StyleSheet,TouchableOpacity } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import LinearGradient from 'react-native-linear-gradient';
import { useDispatch } from 'react-redux';

import { navigateBundling, navigatePascabayar, navigateStarterpack } from '../store/actions/productAction';

export default function SliderHome() {
  const dispatch = useDispatch();

  return (
    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
      <TouchableOpacity onPress={() => dispatch(navigateBundling(0))}>
        <LinearGradient
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 0 }}
          colors={['#08038a', '#4842d6']}
          style={styles.boxSlider}>
          <Text style={styles.contentSlider}>Smartphone</Text>
        </LinearGradient>
      </TouchableOpacity>
      <TouchableOpacity onPress={() => dispatch(navigateBundling(1))}>
        <LinearGradient
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 0 }}
          colors={['#910000', '#cc3737']}
          style={styles.boxSlider}>
          <Text style={styles.contentSlider}>Orbit</Text>
        </LinearGradient>
      </TouchableOpacity >
      <TouchableOpacity onPress={() => dispatch(navigateStarterpack(0))}>
        <LinearGradient
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 0 }}
          colors={['#005e26', '#40a368']}
          style={styles.boxSlider}>
          <Text style={styles.contentSlider}>Simpati</Text>
        </LinearGradient>
      </TouchableOpacity>
      <TouchableOpacity onPress={() => dispatch(navigateStarterpack(2))}>
        <LinearGradient
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 0 }}
          colors={['#ad4d03', '#f08d41']}
          style={styles.boxSlider}>
          <Text style={styles.contentSlider}>Loop</Text>
        </LinearGradient>
      </TouchableOpacity>
      <TouchableOpacity onPress={() => dispatch(navigateStarterpack(1))}>
        <LinearGradient
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 0 }}
          colors={['#e52d27', '#b31217']}
          style={styles.boxSlider}>
          <Text style={styles.contentSlider}>KartuAs</Text>
        </LinearGradient>
      </TouchableOpacity>
      <TouchableOpacity onPress={() => dispatch(navigatePascabayar(0))}>
        <LinearGradient
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 0 }}
          colors={['#7b0080', '#b543ba']}
          style={styles.boxSlider}>
          <Text style={styles.contentSlider}>Halo Play</Text>
        </LinearGradient>
      </TouchableOpacity>
      <TouchableOpacity onPress={() => dispatch(navigatePascabayar(1))}>
        <LinearGradient
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 0 }}
          colors={['#1f19c2', '#7772f7']}
          style={styles.boxSlider}>
          <Text style={styles.contentSlider}>HaloUnlimited</Text>
        </LinearGradient>
      </TouchableOpacity>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  boxSlider: {
    height: 40,
    width: 120,
    borderRadius: 100,
    marginVertical: 20,
    marginHorizontal: 5,
    justifyContent: 'center',
    alignItems: 'center'
  },
  contentSlider: {
    color: 'white',
    fontWeight: 'bold'
  }
});
